#include <iostream>
#include <thread>
#include <chrono>

using namespace std;
using namespace chrono;

static const int NUM_DATA = 16;
static const int NUM_THREADS = 4;
int sum;


bool choosing[NUM_THREADS];  // 번호표
int number[NUM_THREADS] = { 0 }; 

int max(int arr[], int n)
{
	int temp = -1;
	for (int i = 0; i < n; ++i)
	{
		if (temp < arr[i])
			temp = arr[i];
	}

	return temp;
}

void lock(int i)
{
	choosing[i] = true; // 번호표 대기
	number[i] = 1 + max(number, NUM_THREADS); // 번호 생성하여 할당
	choosing[i] = false; // 번호표 받음

	for (int j = 0; j < NUM_THREADS; ++j) // 모든 쓰레드 비교
	{
		while(choosing[j]){ } // 번호표 받을때까지 대기
		while((number[j]!=0) && ((number[j], j) < (number[i], i))) { }
		//번호표 받았고, 
		//j의 번호가 i의 번호보다 작거나 또는 번호표가 같을때 j 가 i보다 작으면
		//j가 종료 할때까지 대기

	}
	// (a < b) < (c,d) 는 (a<c) or (a=c and b<d) 라는 의미
}

void unlock(int i)
{
	number[i] = 0;
}

void compute_thread(int i) //                             NUM_DATA / NUM_THREADS
{
	while (true)
	{

		lock(i);
		//임계구역
		for (int j = 0; j < 250000000; ++j) sum += 1;

		unlock(i);
		//임계구역 x
		break;
		
	}

}

int main()
{
	thread *worker[NUM_THREADS];

	auto start = high_resolution_clock::now();
	for (int i = 0; i < NUM_THREADS; ++i)
	{
		worker[i] = new thread{ compute_thread, i };
	}


	for (auto &item : worker)
		item->join();

	auto end = high_resolution_clock::now();
	auto du = end - start;

	for (auto &item : worker)
		delete item;


	cout << sum;
	cout << "\n " << duration_cast<milliseconds>(du).count() << " milliseconds\n";
}