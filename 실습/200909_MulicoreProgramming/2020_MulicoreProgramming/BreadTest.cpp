#include <iostream>
#include <thread>
using namespace std;

const int PNUM = 10;		// 프로세스의 생성 개수 상수화 , 프로세스의 개수가 늘고 줆에 있어 이 수치만 수정하면 된다
const int N = 5;			// 연상의 계산을 하기위한 상수
int n; // 공용 변수


int number[PNUM];		// 프로세스의 티켓의 값을 저장할 배열
int choosing[PNUM];	// 프로세스가 번호표를 뽑았는지 아닌지를 기억하는지 저장할 배열

// PNUM : 프로세스의 개수 
int MAX()		// 티켓을 나눠주는 함수. 티켓이 저장되어 있는 number 배열의 값들을 조사하여서 가장 큰 값을 반환하여 주는 역할
{
	int MAXNUM = 0;
	for (int i = 0; i < PNUM; ++i)
	{
		for (int j = 0; j < PNUM; ++j)
		{
			if (number[i] > number[j])
			{
				MAXNUM = number[j];
			}
		}
	}

	return MAXNUM;
}

int NUM(int j, int i)
{
	if (number[j] < number[i])
		return 1;

	else if (number[j] > number[i])
		return 0;

	else
	{
		if (j < i)
			return 1;
		else if (j > i)
			return 0;
	}
}

void Enter_Bakery(int i)
{
	choosing[i] = true;
	number[i] = MAX() + 1;
	choosing[i] = false;

	for (int j = 0; j < PNUM; ++j)
	{
		while (choosing[j])
		{
			//cout << j << " Process is Waiting until get a ticket" << endl;
		}
		while ((number[j] != 0) && (NUM(j, i)))
		{
			//cout << j << "Process is Busy Waiting" << endl;
		}
	}

	cout << i << "process's ticket " << number[i] << endl; // 현재 수행하고 있는 프로세스의 티켓번호 출력

	n = 0;

	cout << i << " process is calculating" << endl;
	for (int k = 0; k <= N; ++k)
		n += 1;

	cout << i << " process is calcualte result is " << n << endl;

}

void Leave_Bakery(int i)
{
	cout << i << "process is calculate complete " << endl;
	number[i] = 0;
}

void process(int process)
{
	Enter_Bakery(process);
	Leave_Bakery(process);
}


int main()
{
	for (int i = 0; i < PNUM; ++i)
	{
		number[i] = 0;
		choosing[i] = true;
	}


	// 2 쓰레드
	thread t0{ process, 3 };
	thread t1{ process, 2 };
	thread t2{ process, 1 };
	thread t3{ process, 0 };
	thread t4{ process, 4 };
	thread t5{ process, 5 };
	thread t6{ process, 6 };
	thread t7{ process, 7 };
	thread t8{ process, 8 };
	thread t9{ process, 9 };


	t0.join();
	t1.join();
	t2.join();
	t3.join();
	t4.join();
	t5.join();
	t6.join();
	t7.join();
	t8.join();
	t9.join();

}