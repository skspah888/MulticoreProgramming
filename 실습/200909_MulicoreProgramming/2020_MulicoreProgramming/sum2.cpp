#include <iostream>
#include <thread>
#include <vector>
#include <mutex>
#include <chrono>
#include <atomic>
using namespace std;
using namespace chrono;

const int MAX_TRHEAD = 16;

volatile int sum[MAX_TRHEAD * 64];
mutex sum_lock;

void thread_func(int t_id, int num_thread)
{
	volatile int local_sum = 0;
	for (int i = 0; i < 50000000 / num_thread; ++i)
	{
		sum[t_id * 64] += 2;
	}
}

int main()
{
	for (int count = 1; count <= MAX_TRHEAD; count *= 2)
	{
		vector<thread> vec_thread;
		for(auto& s : sum) 
			s = 0;

		auto start_time = high_resolution_clock::now();

		for (int i = 0; i < count; ++i)
			vec_thread.emplace_back(thread_func, i, count);

		for (auto& t : vec_thread)
			t.join();

		int t_sum = 0;
		for (auto s : sum)
			t_sum += s;

		auto end_time = high_resolution_clock::now();
		auto exec_time = end_time - start_time;

		cout << "Number of threads = " << count << " , ";
		cout << "Exec Time = " << duration_cast<milliseconds>(exec_time).count() << "ms , ";
		cout << "Sum = " << t_sum << endl;
	}
}