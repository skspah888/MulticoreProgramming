#include <iostream>
#include <thread>
#include <vector>
#include <mutex>
#include <chrono>
#include <atomic>
using namespace std;
using namespace chrono;

const int MAX_TRHEAD = 8;
const int MAX_NUMBER = 100000000;

volatile int SUM = 0;

mutex sum_lock;

bool choosing[100];  // 번호표
int number[100] = { 0 };

int max(int arr[], int n)
{
	int temp = -1;
	for (int i = 0; i < n; ++i)
	{
		if (temp < arr[i])
			temp = arr[i];
	}

	return temp;
}

int MAX(int count)		// 티켓을 나눠주는 함수. 티켓이 저장되어 있는 number 배열의 값들을 조사하여서 가장 큰 값을 반환하여 주는 역할
{
	int MAXNUM = 0;
	for (int i = 0; i < count; ++i)
	{
		for (int j = 0; j < count; ++j)
		{
			if (number[i] > number[j])
			{
				MAXNUM = number[j];
			}
		}
	}

	return MAXNUM;
}


void Bread_lock(int i, int count)
{
	choosing[i] = true; // 번호표 대기
	//number[i] = 1 + max(number, count); // 번호 생성하여 할당
	number[i] = 1 + MAX(count); // 번호 생성하여 할당
	choosing[i] = false; // 번호표 받음

	for (int j = 0; j < count; ++j) // 모든 쓰레드 비교
	{
		while (choosing[j]) {} // 번호표 받을때까지 대기
		while ((number[j] != 0) && ((number[j], j) < (number[i], i))) {}
		//번호표 받았고, 
		//j의 번호가 i의 번호보다 작거나 또는 번호표가 같을때 j 가 i보다 작으면
		//j가 종료 할때까지 대기

	}
	// (a < b) < (c,d) 는 (a<c) or (a=c and b<d) 라는 의미
}

void Bread_unlock(int i)
{
	number[i] = 0;
}

void thread_NoLock(int t_id, int num_thread)
{
	for (int i = 0; i < MAX_NUMBER / num_thread; ++i)
	{
		SUM += 1;
	}
}

void thread_Mutex(int t_id, int num_thread)
{
	for (int i = 0; i < MAX_NUMBER / num_thread; ++i)
	{
		sum_lock.lock();
		SUM += 1;
		sum_lock.unlock();
	}
}

void thread_Bread(int t_id, int num_thread)
{
	for (int i = 0; i < MAX_NUMBER / num_thread; ++i)
	{
		Bread_lock(t_id, num_thread);
		SUM += 1;
		Bread_unlock(t_id);
	}
}

int main()
{
	for (int count = 1; count <= MAX_TRHEAD; count *= 2)
	{
		vector<thread> vec_thread;

		auto start_time = high_resolution_clock::now();

		for (int i = 0; i < count; ++i)
			vec_thread.emplace_back(thread_Bread, i, count);

		for (auto& t : vec_thread)
			t.join();


		auto end_time = high_resolution_clock::now();
		auto exec_time = end_time - start_time;

		cout << "Number of threads = " << count << " , ";
		cout << "Exec Time = " << duration_cast<milliseconds>(exec_time).count() << "ms , ";
		cout << "Sum = " << SUM << endl;


		// 초기화
		SUM = 0;

		for (auto& p : choosing)
			p = true;

		for (auto& p : number)
			p = 0;
	}
}