#include <iostream>
#include <thread>
#include <vector>
#include <mutex>
#include <chrono>
#include <atomic>
using namespace std;
using namespace chrono;

atomic <int> sum = 0;
mutex sum_lock;

void thread_func(int num_thread)
{
	for (int i = 0; i < 50000000 / num_thread; ++i)
	{
		/*sum_lock.lock();
		sum += 2;
		sum_lock.unlock();*/

		//_asm lock add sum, 2;

		sum += 2;
	}
}

int main()
{
	for (int count = 1; count <= 16; count *= 2)
	{
		vector<thread> vec_thread;
		sum = 0;
		auto start_time = high_resolution_clock::now();

		for (int i = 0; i < count; ++i)
			vec_thread.emplace_back(thread_func, count);

		for (auto& t : vec_thread)
			t.join();

		auto end_time = high_resolution_clock::now();
		auto exec_time = end_time - start_time;

		cout << "Number of threads = " << count << " , ";
		cout << "Exec Time = " << duration_cast<milliseconds>(exec_time).count() << "ms , ";
		cout << "Sum = " << sum << endl;
	}
}