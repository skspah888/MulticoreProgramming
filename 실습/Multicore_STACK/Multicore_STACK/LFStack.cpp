#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <chrono>
#include <atomic>
using namespace std;
using namespace chrono;

constexpr int NUM_TEST = 10000000;
constexpr int MAX_THREAD = 16;

///// Lock Free Queue

thread_local int thread_id;
int num_threads;

class NODE
{
public:
	volatile int key;
	NODE* volatile next;

	NODE()
	{
		key = 0;
		next = nullptr;
	}
	NODE(int key_value)
	{
		key = key_value;
		next = nullptr;
	}
	~NODE() {}
};

class null_mutex
{
public:
	void lock() {}
	void unlock() {}
};

class LFSTACK
{
	NODE* volatile top;

public:
	LFSTACK() : top(nullptr) { }
	~LFSTACK()
	{
		clear();
	};

	void clear()
	{
		while (nullptr != top) {
			NODE* to_delete = top;
			top = top->next;
			delete to_delete;
		}
	}

	bool CAS(NODE* volatile* top, NODE* old_top, NODE* new_top)
	{
		return atomic_compare_exchange_strong(
			reinterpret_cast<atomic_int volatile*>(top),
			reinterpret_cast<int*>(&old_top),
			reinterpret_cast<int>(new_top)
		);
	}

	void Push(int key)
	{
		NODE* e = new NODE{ key };
		while (true)
		{
			NODE* first = top;
			e->next = first;
			if (true == CAS(&top, first, e))
				return;
		}
	}

	int Pop()
	{
		while (true)
		{
			NODE* first = top;
			if (nullptr == first) return -1;			// EMPTY!!

			int value = first->key;
			if (true == CAS(&top, first, first->next))
			{
				//delete first;			// ABA문제로 일단 Delete하지 않는다
				return value;
			}
		}
	}

	void display20()
	{
		NODE* ptr = top;
		for (int i = 0; i < 20; ++i) {
			if (nullptr == ptr) break;

			cout << ptr->key << ", ";
			ptr = ptr->next;
		}
		cout << endl;
	}
};


LFSTACK my_stack;

void benchmark(int t_id)
{
	const int loop = NUM_TEST / num_threads;
	thread_id = t_id;
	for (int i = 0; i < loop; ++i)
	{
		if ((rand() % 2 == 0) || (i < 1000 / num_threads))
			my_stack.Push(i);
		else
			my_stack.Pop();
	}
}

int main()
{
	// 벤치마크 프로그램 실행
	for (int num = 1; num <= MAX_THREAD; num = num * 2)
	{
		num_threads = num;
		vector<thread> workers;
		my_stack.clear();

		auto start_t = high_resolution_clock::now();

		for (int i = 0; i < num; ++i)
			workers.emplace_back(benchmark, i);

		for (auto& th : workers)
			th.join();

		auto end_t = high_resolution_clock::now();
		auto exec_time = end_t - start_t;

		cout << "Number of threads = " << num;
		cout << ",  Exec Time = " << duration_cast<milliseconds>(exec_time).count() << "ms" << endl;
		my_stack.display20();
		cout << endl;
	}
}