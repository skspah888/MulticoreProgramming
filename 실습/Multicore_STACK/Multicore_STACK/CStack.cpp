#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <chrono>
#include <atomic>
using namespace std;
using namespace chrono;

constexpr int NUM_TEST = 10000000;
constexpr int MAX_THREAD = 16;

///// CStack

thread_local int thread_id;
int num_threads;

class NODE
{
public:
	volatile int key;
	NODE* volatile next;

	NODE()
	{
		key = 0;
		next = nullptr;
	}
	NODE(int key_value)
	{
		key = key_value;
		next = nullptr;
	}
	~NODE() {}
};

class null_mutex
{
public:
	void lock() {}
	void unlock() {}
};


class CSTACK
{
	NODE* volatile top;
	mutex s_lock;

public:
	CSTACK() : top(nullptr) { }
	~CSTACK()
	{
		clear();
	};

	void clear()
	{
		while (nullptr != top) {
			NODE* to_delete = top;
			top = top->next;
			delete to_delete;
		}
	}

	void Push(int key)
	{
		NODE* e = new NODE{ key };
		s_lock.lock();
		e->next = top;
		top = e;
		s_lock.unlock();
	}

	int Pop()
	{
		s_lock.lock();
		if (nullptr == top)
		{
			s_lock.unlock();
			return -1;
		}
		NODE* p = top;
		top = top->next;
		int value = p->key;
		s_lock.unlock();
		delete p;
		return value;
	}

	void display20()
	{
		NODE* ptr = top;
		for (int i = 0; i < 20; ++i) {
			if (nullptr == ptr) break;

			cout << ptr->key << ", ";
			ptr = ptr->next;
		}
		cout << endl;
	}
};

CSTACK my_stack;

void benchmark(int t_id)
{
	const int loop = NUM_TEST / num_threads;
	thread_id = t_id;
	for (int i = 0; i < loop; ++i)
	{
		if ((rand() % 2 == 0) || (i < 1000 / num_threads))
			my_stack.Push(i);
		else
			my_stack.Pop();
	}
}

int main()
{
	// 벤치마크 프로그램 실행
	for (int num = 1; num <= MAX_THREAD; num = num * 2)
	{
		num_threads = num;
		vector<thread> workers;
		my_stack.clear();

		auto start_t = high_resolution_clock::now();

		for (int i = 0; i < num; ++i)
			workers.emplace_back(benchmark, i);

		for (auto& th : workers)
			th.join();

		auto end_t = high_resolution_clock::now();
		auto exec_time = end_t - start_t;

		cout << "Number of threads = " << num;
		cout << ",  Exec Time = " << duration_cast<milliseconds>(exec_time).count() << "ms" << endl;
		my_stack.display20();
		cout << endl;
	}
}