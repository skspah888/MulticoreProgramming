#include <thread>
#include <iostream>
using namespace std;

constexpr int SIZE = 25000000;
volatile int* bound;
volatile bool g_done = false;
int g_error = 0;

void trace_0()
{
	for (int i = 0; i < SIZE; i++)
	{
		*bound = -(1 + *bound);
	}
	g_done = true;
}

void trace_1()
{
	while (false == g_done)
	{
		int v = *bound;
		if ((v != 0) && (v != -1))
			g_error++;
	}
}

int main()
{
	int a[64];
	int addr = reinterpret_cast<int>(&a[31]);
	addr = (addr / 64) * 64;
	addr = addr - 2;
	bound = reinterpret_cast<int*>(addr);
	*bound = 0;
	thread core_0{ trace_0 };
	thread core_1{ trace_1 };

	core_0.join();
	core_1.join();

	cout << "Number of invalid value " << g_error << endl;
}