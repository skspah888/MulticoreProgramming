#include <thread>
#include <iostream>
#include <memory>
#include <atomic>
using namespace std;

constexpr int SIZE = 50000000;
atomic_int x, y;
atomic_int trace_x[SIZE], trace_y[SIZE];

void trace_0()
{
	for (int i = 0; i < SIZE; ++i)
	{
		x = i;
		//atomic_thread_fence(std::memory_order_seq_cst);
		trace_y[i].store(y);
	}
}

void trace_1()
{
	for (int i = 0; i < SIZE; ++i)
	{
		y = i;
		//atomic_thread_fence(std::memory_order_seq_cst);
		trace_x[i].store(x);
	}
}

/*
		 실습 15 :  메모리 일관성 
*/

int main()
{
	int m_error = 0;

	thread core_0{ trace_0 };
	thread core_1{ trace_1 };
	core_0.join();
	core_1.join();

	for (int i = 0; i < SIZE-1; ++i)
	{
		if (trace_x[i] == trace_x[i + 1])
		{
			if (trace_y[trace_x[i]] == trace_y[trace_x[i] + 1])
			{
				if (trace_y[trace_x[i]] == i)
					m_error++;
					
			}
		}
	}

	cout << "number of memory inconsistency : " << m_error << endl;

}