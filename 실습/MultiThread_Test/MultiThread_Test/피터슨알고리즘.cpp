#include <iostream>
#include <thread>
#include <mutex>
#include <chrono>
#include <memory>
using namespace std;
using namespace chrono;

volatile int sum;
mutex sum_lock;

volatile bool flag[2] = { false, false };
volatile int victim;	// 데드락 방지

void p_lock(const int t_id)
{
	int other = 1 - t_id;			// 상대방 구하기
	flag[t_id] = true;
	victim = t_id;					// 데드락 방지
	//_asm mfence;
	atomic_thread_fence(std::memory_order_seq_cst);
	while (true == flag[other] && (t_id == victim));	
}

void p_unlock(const int t_id)
{
	flag[t_id] = false;
}

void worker(const int t_id)
{
	for (int i = 0; i < 50000000; ++i)
	{
		//sum_lock.lock();
		//p_lock(t_id);
		sum += 2;
		//sum_lock.unlock();
		//p_unlock(t_id);
	}
}

int main()
{
	auto start_t = high_resolution_clock::now();

	//// 싱글 쓰레드
	//worker(0);
	//worker(1);

	// 2 쓰레드
	thread t1{ worker, 0 };
	thread t2{ worker, 1 };
	t2.join();
	t1.join();

	auto end_t = high_resolution_clock::now();
	auto exec_time = end_t - start_t;

	cout << "Exec Time : " << duration_cast<milliseconds>(exec_time).count() << "ms" << endl;
	cout << "Sum = " << sum << endl;
}