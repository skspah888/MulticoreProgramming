#include <iostream>
#include <thread>
#include <mutex>
#include <chrono>
#include <memory>
#include <vector>
#include <atomic>
using namespace std;
using namespace chrono;

volatile int sum;
mutex sum_lock;

volatile int LOCK = 0;

bool CAS(volatile int* addr, int expected, int new_value)
{
	return atomic_compare_exchange_strong(
		reinterpret_cast<volatile atomic_int*>(addr),
		&expected, new_value);
}

void cas_lock()
{
	// 성공을 하면서 LOCK에 1을 쓴다 => 다른 쓰레드는 실패니 무한루프에 빠짐
	while (false == CAS(&LOCK, 0, 1));
}

void cas_unlock()
{
	LOCK = 0;
}

void worker(const int num_threads)
{
	for (int i = 0; i < 50000000 / num_threads; ++i)
	{
		cas_lock();
		//sum_lock.lock();
		sum += 2;
		cas_unlock();
		//sum_lock.unlock();
	}
}

int main()
{
	int num = 1;
	for (num = 1; num <= 16; num *= 2)
	{
		vector<thread> workers;
		sum = 0;
		auto start_t = high_resolution_clock::now();

		for (int i = 0; i < num; ++i)
			workers.emplace_back(worker, num);

		for (auto& th : workers)
			th.join();
	

	auto end_t = high_resolution_clock::now();
	auto exec_time = end_t - start_t;

	cout << "Number of threads : " << num << ",   ";
	cout << "Exec Time : " << duration_cast<milliseconds>(exec_time).count() << "ms, ";
	cout << "Sum = " << sum << endl;
	}

}