#include <iostream>
#include <thread>
using namespace std;

volatile int g_data = 0;
volatile bool data_ready = false;

void Receiver()
{
	while (false == data_ready);
	int my_Data = g_data;
	cout << "Received [" << my_Data << "]" << endl;
}

void Sender()
{
	g_data = 999;
	data_ready = true;
	cout << "Sent\n";
}

int main()
{
	thread R{ Receiver };
	thread S{ Sender };
	R.join();
	S.join();
}