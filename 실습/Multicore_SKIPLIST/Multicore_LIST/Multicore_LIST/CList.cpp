#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <chrono>
using namespace std;
using namespace chrono;

constexpr int NUM_TEST = 4000000;
constexpr int MAX_THREAD = 8;
constexpr int KEY_RANGE = 1000;

// 성난 동기화 LIST
// 실습 19

class NODE
{
public:
	int key; 
	NODE* next;

	NODE() 
	{ 
		next = NULL; 
	}
	NODE(int key_value) 
	{ 
		next = NULL; 
		key = key_value; 
	}
	~NODE() {}
};

class CLIST 
{
	NODE head, tail; 
	mutex glock; 

public: 
	CLIST() 
	{ 
		head.key = 0x80000000; 
		tail.key = 0x7FFFFFFF; 
		head.next = &tail; 
	} 
	~CLIST() {}

	void Init() 
	{ 
		NODE* ptr; 
		while (head.next != &tail) 
		{ 
			ptr = head.next; 
			head.next = head.next->next; 
			delete ptr; 
		} 
	} 

	bool Add(int key) 
	{
		NODE* pred, * curr;
		pred = &head; glock.lock();
		curr = pred->next;

		while (curr->key < key)
		{
			pred = curr;
			curr = curr->next;
		}

		if (key == curr->key)
		{
			glock.unlock();
			return false;
		}

		else
		{
			NODE* node = new NODE(key);
			node->next = curr;
			pred->next = node;
			glock.unlock();
			return true;
		}
	} 
	bool Remove(int key) 
	{
		NODE* pred, * curr;
		pred = &head;
		glock.lock();
		curr = pred->next;

		while (curr->key < key)
		{
			pred = curr;
			curr = curr->next;
		}

		if (key == curr->key)
		{
			pred->next = curr->next;
			delete curr;
			glock.unlock();
			return true;
		}
		else
		{
			glock.unlock();
			return false;
		}
	}

	bool Contains(int key) 
	{
		NODE* pred, * curr;
		pred = &head;
		glock.lock();
		curr = pred->next;
		while (curr->key < key)
		{
			pred = curr;
			curr = curr->next;
		}

		if (key == curr->key)
		{
			glock.unlock();
			return true;
		}

		else
		{
			glock.unlock();
			return false;
		}
	}

	void print_first_20()
	{
		NODE* curr = head.next;
		for (int i = 0; i < 20; ++i)
		{
			if (&tail == curr)
				break;
			cout << curr->key << ", ";
			curr = curr->next;
		}
		cout << endl;
	}
};

CLIST clist;

void ThreadFunc(int num_thread) {
	int key;
	for (int i = 0; i < NUM_TEST / num_thread; i++) {
		switch (rand() % 3)
		{
		case 0:
			key = rand() % KEY_RANGE;
			clist.Add(key);
			break;

		case 1:
			key = rand() % KEY_RANGE;
			clist.Remove(key);
			break;

		case 2:
			key = rand() % KEY_RANGE;
			clist.Contains(key);
			break;

		default:
			cout << "Error\n";
			exit(-1);
		}
	}
}

int main()
{
	for (int num = 1; num <= MAX_THREAD; num = num * 2)
	{
		clist.Init();

		// 벤치마크 프로그램 실행
		vector<thread> workers;

		auto start_t = high_resolution_clock::now();

		for (int i = 0; i < num; ++i)
			workers.emplace_back(ThreadFunc, num);

		for (auto& th : workers)
			th.join();

		auto end_t = high_resolution_clock::now();
		auto exec_time = end_t - start_t;

		cout << "Number of threads = " << num;
		cout << ",  Exec Time = " << duration_cast<milliseconds>(exec_time).count() << "ms" << endl;
		clist.print_first_20(); // 맨 앞에있는 20개의 원소 출력 => Sorting이 되어있는 값이 출력되어야함
								// 중간 중간 값이 비어있을수 있지만, 같은 값이 출력되면 안됨

		cout << endl;
	}
}