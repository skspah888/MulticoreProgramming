#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <chrono>
using namespace std;
using namespace chrono;

constexpr int NUM_TEST = 4000000;
constexpr int MAX_THREAD = 8;
constexpr int KEY_RANGE = 1000;

// 낙천적인 동기화
// 실습 21

class NODE
{
public:
	int key;
	NODE* next;
	mutex n_lock;

	NODE()
	{
		next = NULL;
	}
	NODE(int key_value)
	{
		next = NULL;
		key = key_value;
	}
	~NODE() {}

	void lock()
	{
		n_lock.lock();
	}

	void unlock()
	{
		n_lock.unlock();
	}

};

class OLIST
{
	NODE head, tail;

public:
	OLIST()
	{
		head.key = 0x80000000;
		tail.key = 0x7FFFFFFF;
		head.next = &tail;
	}
	~OLIST() {}

	void Init()
	{
		NODE* ptr;
		while (head.next != &tail)
		{
			ptr = head.next;
			head.next = head.next->next;
			delete ptr;
		}
	}

	//// 유효성 검사 : 원래는 key를 보고 하지만 굳이 할 필요없음
	//bool is_valid(int key, NODE* pred2, NODE* curr2)
	//{
	//	// 다시 처음부터 검사해서 
	//	NODE* pred = &head;
	//	NODE* curr = pred->next;
	//	while (curr->key < key) {
	//		pred = curr;
	//		curr = curr->next;
	//	}

	//	return ((pred == pred2) && (curr == curr2));
	//}

	bool is_valid(NODE* pred, NODE* curr)
	{
		// node의 key가 pred보다 크면 값이 없는 것이다.
		NODE* node = &head;
		while (node->key <= pred->key)
		{
			if (node == pred)
				return (pred->next == curr);
			node = node->next;
		}
		return false;
	}

	bool Add(int key)
	{
		while (true)
		{
			NODE* pred, *curr;
			pred = &head;
			curr = pred->next;

			while (curr->key < key)
			{
				pred = curr;
				curr = curr->next;
			}

			// 수정하기전 Lock걸기
			pred->lock();
			curr->lock();

			if (is_valid(pred, curr))
			{
				if (key == curr->key)
				{
					curr->unlock();
					pred->unlock();
					return false;
				}

				else
				{
					NODE* node = new NODE(key);
					node->next = curr;
					pred->next = node;

					pred->unlock();
					curr->unlock();

					return true;
				}
			}
			else
			{
				curr->unlock();
				pred->unlock();
			}
		}
	}
	bool Remove(int key)
	{
		while (true)
		{
			NODE* pred, *curr;
			pred = &head;
			curr = pred->next;

			while (curr->key < key)
			{
				pred = curr;
				curr = curr->next;
			}

			// 수정하기전 Lock걸기
			pred->lock();
			curr->lock();

			if (is_valid(pred, curr))
			{
				if (key == curr->key)
				{
					pred->next = curr->next;
					curr->unlock();
					pred->unlock();
					//delete curr;

					return true;
				}
				else
				{
					curr->unlock();
					pred->unlock();
					return false;
				}
			}

			else
			{
				curr->unlock();
				pred->unlock();
			}
		}
	}

	bool Contains(int key)
	{
		while (true)
		{
			NODE* pred, *curr;
			pred = &head;
			curr = pred->next;

			while (curr->key < key)
			{
				pred = curr;
				curr = curr->next;
			}

			// 수정하기전 Lock
			pred->lock();
			curr->lock();

			if (is_valid(pred, curr))
			{
				if (key == curr->key)
				{
					pred->unlock();
					curr->unlock();
					return true;
				}

				else
				{
					pred->unlock();
					curr->unlock();
					return false;
				}
			}
			else
			{
				curr->unlock();
				pred->unlock();
			}
		}
	}

	void print_first_20()
	{
		NODE* curr = head.next;
		for (int i = 0; i < 20; ++i)
		{
			if (&tail == curr)
				break;
			cout << curr->key << ", ";
			curr = curr->next;
		}
		cout << endl;
	}
};

OLIST flist;

void ThreadFunc(int num_thread) {
	int key;
	for (int i = 0; i < NUM_TEST / num_thread; i++) {
		switch (rand() % 3)
		{
		case 0:
			key = rand() % KEY_RANGE;
			flist.Add(key);
			break;

		case 1:
			key = rand() % KEY_RANGE;
			flist.Remove(key);
			break;

		case 2:
			key = rand() % KEY_RANGE;
			flist.Contains(key);
			break;

		default:
			cout << "Error\n";
			exit(-1);
		}
	}
}

int main()
{
	for (int num = 1; num <= MAX_THREAD; num = num * 2)
	{
		flist.Init();

		// 벤치마크 프로그램 실행
		vector<thread> workers;

		auto start_t = high_resolution_clock::now();

		for (int i = 0; i < num; ++i)
			workers.emplace_back(ThreadFunc, num);

		for (auto& th : workers)
			th.join();

		auto end_t = high_resolution_clock::now();
		auto exec_time = end_t - start_t;

		cout << "Number of threads = " << num;
		cout << ",  Exec Time = " << duration_cast<milliseconds>(exec_time).count() << "ms" << endl;
		flist.print_first_20();

		cout << endl;
	}
}