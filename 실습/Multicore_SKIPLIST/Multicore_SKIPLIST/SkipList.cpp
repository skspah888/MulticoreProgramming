#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <chrono>
#include <atomic>
using namespace std;
using namespace chrono;

constexpr int NUM_TEST = 4000000;
constexpr int MAX_THREAD = 16;
constexpr int KEY_RANGE = 1000;

/////////////////////////////////// Skip List 구현
constexpr int MAX_LEVEL = 8;

class SKNODE
{
public:
	int key;
	SKNODE* next[MAX_LEVEL + 1];
	int top_level;

	// head, tail을 만들때 기본 생성자가 호출되기 때문
	SKNODE()
	{
		for (auto& n : next) n = nullptr;
		top_level = MAX_LEVEL;
	}
	SKNODE(int x, int top)
	{
		for (int i = 0; i <= top; ++i)
			next[i] = nullptr;
		top_level = top;
		key = x;
	}

};

class SKLIST
{
public:
	SKNODE head, tail;
	mutex sk_l;

	SKLIST()
	{
		head.key = 0x80000000; // -MAXINT
		tail.key = 0x7FFFFFFF; // +MAXINT
		for (auto& n : head.next)
			n = &tail;

		// SKNODE의 기본 생성자에서 했기때문에 굳이 안해도 된다.
		head.top_level = tail.top_level = MAX_LEVEL;
	}
	~SKLIST()
	{
		init();
	}

public:
	void init()
	{
		SKNODE* p = head.next[0];
		while (&tail != p)
		{
			SKNODE* todelete = p;
			p = p->next[0];
			delete todelete;
		}

		for (auto& n : head.next) n = &tail;
	}

	void find(int x, SKNODE* preds[], SKNODE* currs[])
	{
		preds[MAX_LEVEL] = &head;
		for (int cl = MAX_LEVEL; cl >= 0; --cl)
		{
			if (MAX_LEVEL != cl)
				preds[cl] = preds[cl + 1];
			currs[cl] = preds[cl]->next[cl];

			while (currs[cl]->key < x)
			{
				preds[cl] = currs[cl];
				currs[cl] = currs[cl]->next[cl];
			}
		}
	}

	bool Add(int x)
	{
		SKNODE* preds[MAX_LEVEL + 1];
		SKNODE* currs[MAX_LEVEL + 1];

		sk_l.lock();
		find(x, preds, currs);

		if (currs[0]->key == x)
		{
			sk_l.unlock();
			return false;
		}
		else
		{
			//int toplevel = rand() % (MAX_LEVEL + 1);		// 구시대적 방법
			// 적절한 개수로 높이 분포를 하기위한 방법
			int toplevel = 0;
			while ((rand() % 2) == 1)
			{
				toplevel++;
				if (toplevel == MAX_LEVEL) break;
			}

			SKNODE* e = new SKNODE(x, toplevel);
			for (int i = 0; i < toplevel; ++i)
			{
				preds[i]->next[i] = e;
				e->next[i] = currs[i];
			}

			sk_l.unlock();
			return true;
		}
	}

	bool Remove(int x)
	{
		// 찾는다
		SKNODE* preds[MAX_LEVEL + 1];
		SKNODE* currs[MAX_LEVEL + 1];

		sk_l.lock();
		find(x, preds, currs);

		// 키가 있다
		if (currs[0]->key == x)
		{
			//print_all();
			//cout << "currs[0]->key : " << currs[0]->key << "  x : " << x << endl;
			//cout << "currs[0]->toplevel : " << currs[0]->top_level << "  maxLevel : " << MAX_LEVEL << endl;

			// 지운다 : curr노드를 지우고, pred노드를 curr->next노드로 연결한다.
			SKNODE* deleteNode = currs[0];

			// 해당 노드의 top_level까지 순회하며 해당 노드 변경
			for (int i = 0; i < currs[0]->top_level; ++i)
				preds[i]->next[i] = currs[i]->next[i];

			delete deleteNode;
			sk_l.unlock();
			return true;
		}

		// 키가 없다 
		else
		{
			sk_l.unlock();
			return false;
		}
	}

	bool Contains(int x)
	{
		SKNODE* preds[MAX_LEVEL + 1];
		SKNODE* currs[MAX_LEVEL + 1];

		sk_l.lock();
		find(x, preds, currs);

		if (currs[0]->key == x)
		{
			sk_l.unlock();
			return true;
		}
		else
		{
			sk_l.unlock();
			return false;
		}
	}

	void print_all()
	{
		SKNODE* p = &head;
		cout << "All Keys : " << endl;
		while (&tail != p)
		{
			p = p->next[0];
			cout << p->key << ", ";
		}
		cout << endl;
	}

	void print_first_20()
	{
		SKNODE* curr = head.next[0];
		for (int i = 0; i < 20; ++i)
		{
			if (&tail == curr->next[0])
				break;
			cout << curr->key << ", ";
			curr = curr->next[0];
		}
		cout << endl;
	}
};



SKLIST flist;

void ThreadFunc(int num_thread) {
	int key;
	for (int i = 0; i < NUM_TEST / num_thread; i++) {
		switch (rand() % 3)
		{
		case 0:
			key = rand() % KEY_RANGE;
			flist.Add(key);
			break;

		case 1:
			key = rand() % KEY_RANGE;
			flist.Remove(key);
			break;

		case 2:
			key = rand() % KEY_RANGE;
			flist.Contains(key);
			/*if (flist.Contains(key))
			{
				flist.print_all();
				cout << "찾은키 : " << key << "진행횟수 : " << i << endl;
				int a = 0;
			}*/
			break;

		default:
			cout << "Error\n";
			exit(-1);
		}
	}
}

int main()
{
	for (int num = 1; num <= MAX_THREAD; num = num * 2)
	{
		flist.init();
		// 벤치마크 프로그램 실행
		vector<thread> workers;

		auto start_t = high_resolution_clock::now();

		for (int i = 0; i < num; ++i)
			workers.emplace_back(ThreadFunc, num);

		for (auto& th : workers)
			th.join();

		auto end_t = high_resolution_clock::now();
		auto exec_time = end_t - start_t;

		cout << "Number of threads = " << num;
		cout << ",  Exec Time = " << duration_cast<milliseconds>(exec_time).count() << "ms" << endl;
		flist.print_first_20();

		cout << endl;
	}
}