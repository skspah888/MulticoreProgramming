#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <chrono>
#include <set>

using namespace std;
using namespace chrono;

constexpr int NUM_TEST = 4000000;
constexpr int MAX_THREAD = 64;
constexpr int KEY_RANGE = 1000;

thread_local int thread_id;

class NODE {
public:
	int key;
	NODE* next;
	bool is_removed;
	mutex n_lock;

	NODE() { key = 0;  next = NULL; is_removed = false; }

	NODE(int key_value) {
		next = NULL;
		is_removed = false;
		key = key_value;
	}

	~NODE() {}

	void lock()
	{
		n_lock.lock();
	}

	void unlock()
	{
		n_lock.unlock();
	}
};

class null_mutex {
public:
	void lock() {}
	void unlock() {}
};

class CLIST {
	NODE head, tail;
	mutex glock;
public:
	CLIST()
	{
		head.key = 0x80000000;
		tail.key = 0x7FFFFFFF;
		head.next = &tail;
	}
	~CLIST() {}

	void Init()
	{
		NODE* ptr;
		while (head.next != &tail) {
			ptr = head.next;
			head.next = head.next->next;
			delete ptr;
		}
	}
	bool Add(int key)
	{
		NODE* pred, * curr;

		pred = &head;
		glock.lock();
		curr = pred->next;
		while (curr->key < key) {
			pred = curr;
			curr = curr->next;
		}

		if (key == curr->key) {
			glock.unlock();
			return false;
		}
		else {
			NODE* node = new NODE(key);
			node->next = curr;
			pred->next = node;
			glock.unlock();
			return true;
		}
	}

	bool Remove(int key)
	{
		NODE* pred, * curr;

		pred = &head;
		glock.lock();
		curr = pred->next;
		while (curr->key < key) {
			pred = curr;
			curr = curr->next;
		}

		if (key == curr->key) {
			pred->next = curr->next;
			delete curr;
			glock.unlock();
			return true;
		}
		else {
			glock.unlock();
			return false;
		}
	}

	bool Contains(int key)
	{
		NODE* pred, * curr;

		pred = &head;
		glock.lock();
		curr = pred->next;
		while (curr->key < key) {
			pred = curr;
			curr = curr->next;
		}
		if (key == curr->key) {
			glock.unlock();
			return true;
		}
		else {
			glock.unlock();
			return false;
		}
	}

	void print_first_20()
	{
		NODE* curr = head.next;
		for (int i = 0; i < 20; ++i) {
			if (&tail == curr) break;
			cout << curr->key << ", ";
			curr = curr->next;
		}
		cout << endl;
	}

};

class FLIST {
	NODE head, tail;
public:
	FLIST()
	{
		head.key = 0x80000000;
		tail.key = 0x7FFFFFFF;
		head.next = &tail;
	}
	~FLIST() {}

	void Init()
	{
		NODE* ptr;
		while (head.next != &tail) {
			ptr = head.next;
			head.next = head.next->next;
			delete ptr;
		}
	}
	bool Add(int key)
	{
		NODE* pred, * curr;

		head.lock();
		pred = &head;
		curr = pred->next;
		curr->lock();
		while (curr->key < key) {
			pred->unlock();
			pred = curr;
			curr = curr->next;
			curr->lock();
		}

		if (key == curr->key) {
			curr->unlock();
			pred->unlock();
			return false;
		}
		else {
			NODE* node = new NODE(key);
			node->next = curr;
			pred->next = node;
			curr->unlock();
			pred->unlock();
			return true;
		}
	}

	bool Remove(int key)
	{
		NODE* pred, * curr;

		head.lock();
		pred = &head;
		curr = pred->next;
		curr->lock();
		while (curr->key < key) {
			pred->unlock();
			pred = curr;
			curr = curr->next;
			curr->lock();
		}

		if (key == curr->key) {
			pred->next = curr->next;
			delete curr;
			pred->unlock();
			//curr->unlock();
			return true;
		}
		else {
			pred->unlock();
			curr->unlock();
			return false;
		}
	}

	bool Contains(int key)
	{
		NODE* pred, * curr;

		head.lock();
		pred = &head;
		curr = pred->next;
		curr->lock();
		while (curr->key < key) {
			pred->unlock();
			pred = curr;
			curr = curr->next;
			curr->lock();
		}
		if (key == curr->key) {
			pred->unlock();
			curr->unlock();
			return true;
		}
		else {
			pred->unlock();
			curr->unlock();
			return false;
		}
	}

	void print_first_20()
	{
		NODE* curr = head.next;
		for (int i = 0; i < 20; ++i) {
			if (&tail == curr) break;
			cout << curr->key << ", ";
			curr = curr->next;
		}
		cout << endl;
	}

};

class OLIST {
	NODE head, tail;
public:
	OLIST()
	{
		head.key = 0x80000000;
		tail.key = 0x7FFFFFFF;
		head.next = &tail;
	}
	~OLIST() {}

	void Init()
	{
		NODE* ptr;
		while (head.next != &tail) {
			ptr = head.next;
			head.next = head.next->next;
			delete ptr;
		}
	}

	bool is_valid(const NODE* pred, const NODE* curr)
	{
		NODE* p = &head;
		while (p->key <= pred->key) {
			if (p == pred)
				return p->next == curr;
			p = p->next;
		}
		return false;
	}

	bool Add(int key)
	{

		NODE* pred, * curr;
		while (true) {
			pred = &head;
			curr = pred->next;
			while (curr->key < key) {
				pred = curr;
				curr = curr->next;
			}
			pred->lock();
			curr->lock();

			if (false == is_valid(pred, curr)) {
				pred->unlock();
				curr->unlock();
				continue;
			}

			if (key == curr->key) {
				curr->unlock();
				pred->unlock();
				return false;
			}
			else {
				NODE* node = new NODE(key);
				node->next = curr;
				pred->next = node;
				curr->unlock();
				pred->unlock();
				return true;
			}
		}
	}

	bool Remove(int key)
	{

		NODE* pred, * curr;
		while (true) {
			pred = &head;
			curr = pred->next;
			while (curr->key < key) {
				pred = curr;
				curr = curr->next;
			}
			pred->lock();
			curr->lock();

			if (false == is_valid(pred, curr)) {
				pred->unlock();
				curr->unlock();
				continue;
			}

			if (key != curr->key) {
				curr->unlock();
				pred->unlock();
				return false;
			}
			else {
				pred->next = curr->next;
				curr->unlock();
				pred->unlock();
				// delete curr;
				return true;
			}
		}
	}

	bool Contains(int key)
	{

		NODE* pred, * curr;
		while (true) {
			pred = &head;
			curr = pred->next;
			while (curr->key < key) {
				pred = curr;
				curr = curr->next;
			}
			pred->lock();
			curr->lock();

			if (false == is_valid(pred, curr)) {
				pred->unlock();
				curr->unlock();
				continue;
			}

			if (key == curr->key) {
				curr->unlock();
				pred->unlock();
				return true;
			}
			else {
				curr->unlock();
				pred->unlock();
				return false;
			}
		}
	}

	void print_first_20()
	{
		NODE* curr = head.next;
		for (int i = 0; i < 20; ++i) {
			if (&tail == curr) break;
			cout << curr->key << ", ";
			curr = curr->next;
		}
		cout << endl;
	}

};

class LLIST {
	NODE head, tail;
public:
	LLIST()
	{
		head.key = 0x80000000;
		tail.key = 0x7FFFFFFF;
		head.next = &tail;
	}
	~LLIST() {}

	void Init()
	{
		NODE* ptr;
		while (head.next != &tail) {
			ptr = head.next;
			head.next = head.next->next;
			delete ptr;
		}
	}

	bool is_valid(const NODE* pred, const NODE* curr)
	{
		return (false == pred->is_removed) &&
			(false == curr->is_removed) &&
			pred->next == curr;
	}

	bool Add(int key)
	{

		NODE* pred, * curr;
		while (true) {
			pred = &head;
			curr = pred->next;
			while (curr->key < key) {
				pred = curr;
				curr = curr->next;
			}
			pred->lock();
			curr->lock();

			if (false == is_valid(pred, curr)) {
				pred->unlock();
				curr->unlock();
				continue;
			}

			if (key == curr->key) {
				curr->unlock();
				pred->unlock();
				return false;
			}
			else {
				NODE* node = new NODE(key);
				node->next = curr;
				pred->next = node;
				curr->unlock();
				pred->unlock();
				return true;
			}
		}
	}

	bool Remove(int key)
	{

		NODE* pred, * curr;
		while (true) {
			pred = &head;
			curr = pred->next;
			while (curr->key < key) {
				pred = curr;
				curr = curr->next;
			}
			pred->lock();
			curr->lock();

			if (false == is_valid(pred, curr)) {
				pred->unlock();
				curr->unlock();
				continue;
			}

			if (key != curr->key) {
				curr->unlock();
				pred->unlock();
				return false;
			}
			else {
				curr->is_removed = true;
				atomic_thread_fence(memory_order_seq_cst);
				pred->next = curr->next;
				curr->unlock();
				pred->unlock();
				// delete curr;
				return true;
			}
		}
	}

	bool Contains(int key)
	{
		NODE* curr = head.next;
		while (curr->key < key)
			curr = curr->next;
		return (curr->key == key) && (false == curr->is_removed);
	}

	void print_first_20()
	{
		NODE* curr = head.next;
		for (int i = 0; i < 20; ++i) {
			if (&tail == curr) break;
			cout << curr->key << ", ";
			curr = curr->next;
		}
		cout << endl;
	}

};

class SPNODE {
public:
	int key;
	shared_ptr<SPNODE> next;
	bool is_removed;
	mutex n_lock;

	SPNODE() { is_removed = false; }

	SPNODE(int key_value) {
		is_removed = false;
		key = key_value;
	}

	~SPNODE() {}

	void lock()
	{
		n_lock.lock();
	}

	void unlock()
	{
		n_lock.unlock();
	}
};

class SPLLIST {
	shared_ptr<SPNODE> head, tail;
public:
	SPLLIST()
	{
		head = make_shared<SPNODE>(0x8000000);
		tail = make_shared<SPNODE>(0x7FFFFFFF);
		head->next = tail;
	}
	~SPLLIST() {}

	void Init()
	{
		head->next = tail;
	}

	bool is_valid(const shared_ptr<SPNODE>& pred, const shared_ptr<SPNODE>& curr)
	{
		return (false == pred->is_removed) &&
			(false == curr->is_removed) &&
			pred->next == curr;
	}

	bool Add(int key)
	{
		shared_ptr<SPNODE> pred, curr;
		while (true) {
			pred = head;
			curr = pred->next;
			while (curr->key < key) {
				pred = curr;
				curr = curr->next;
			}
			pred->lock();
			curr->lock();

			if (false == is_valid(pred, curr)) {
				pred->unlock();
				curr->unlock();
				continue;
			}

			if (key == curr->key) {
				curr->unlock();
				pred->unlock();
				return false;
			}
			else {
				shared_ptr<SPNODE> node = make_shared<SPNODE>(key);
				node->next = curr;
				pred->next = node;
				curr->unlock();
				pred->unlock();
				return true;
			}
		}
	}

	bool Remove(int key)
	{

		shared_ptr<SPNODE> pred, curr;
		while (true) {
			pred = head;
			curr = pred->next;
			while (curr->key < key) {
				pred = curr;
				curr = curr->next;
			}
			pred->lock();
			curr->lock();

			if (false == is_valid(pred, curr)) {
				pred->unlock();
				curr->unlock();
				continue;
			}

			if (key != curr->key) {
				curr->unlock();
				pred->unlock();
				return false;
			}
			else {
				curr->is_removed = true;
				atomic_thread_fence(memory_order_seq_cst);
				pred->next = curr->next;
				curr->unlock();
				pred->unlock();
				// delete curr;
				return true;
			}
		}
	}

	bool Contains(int key)
	{
		shared_ptr<SPNODE> curr = head->next;
		while (curr->key < key)
			curr = curr->next;
		return (curr->key == key) && (false == curr->is_removed);
	}

	void print_first_20()
	{
		shared_ptr<SPNODE> curr = head->next;
		for (int i = 0; i < 20; ++i) {
			if (tail == curr) break;
			cout << curr->key << ", ";
			curr = curr->next;
		}
		cout << endl;
	}

};

class LFNODE;

class CPTR
{
	atomic_int ptr;
public:
	CPTR() {
		ptr = 0;
	}
	CPTR(LFNODE* addr, bool marking)
	{
		int next = reinterpret_cast<int>(addr);
		if (true == marking)
			next = next | 0x01;
		ptr = next;
	}

	void store(LFNODE* addr, bool marking)
	{
		int next = reinterpret_cast<int>(addr);
		if (true == marking)
			next = next | 0x01;
		ptr = next;
	}
	~CPTR() {}
	LFNODE* get_addr()
	{
		return reinterpret_cast<LFNODE*>(ptr & 0xFFFFFFFE);
	}

	bool is_removed()
	{
		return 1 == (ptr & 0x01);
	}

	LFNODE* get_addr(bool* marking)
	{
		int next = ptr;
		*marking = (0x01 == (next & 0x01));
		return reinterpret_cast<LFNODE*>(next & 0xFFFFFFFE);
	}

	bool CAS(LFNODE* old_addr, LFNODE* new_addr, bool old_mark, bool new_mark)
	{
		int old_next = reinterpret_cast<int>(old_addr);
		if (true == old_mark) old_next++;
		int new_next = reinterpret_cast<int>(new_addr);
		if (true == new_mark) new_next++;
		return atomic_compare_exchange_strong(&ptr, &old_next, new_next);
	}
};

class LFNODE {
public:
	int key;
	CPTR next;

	LFNODE() { }

	LFNODE(int key_value) {
		key = key_value;
	}

	~LFNODE() {}
};

class LFLIST {
	LFNODE head, tail;
public:
	LFLIST()
	{
		head.key = 0x80000000;
		tail.key = 0x7FFFFFFF;
		head.next.store(&tail, false);
	}
	~LFLIST() {}

	void Init()
	{
		LFNODE* ptr;
		while (head.next.get_addr() != &tail) {
			ptr = head.next.get_addr();
			head.next.store(ptr->next.get_addr(), false);
			delete ptr;
		}
	}

	void FIND(LFNODE*& pred, LFNODE*& curr, int key)
	{
	retry:
		pred = &head;
		curr = pred->next.get_addr();
		while (true) {
			// marking된 curr지우기
			bool is_removed;
			LFNODE* succ = curr->next.get_addr(&is_removed);
			while (true == is_removed)
				if (true == pred->next.CAS(curr, succ, false, false)) {
					curr = succ;
					succ = curr->next.get_addr(&is_removed);
				}
				else
					goto retry;

			if (curr->key >= key) return;
			pred = curr;
			curr = succ;
		}
	}

	bool Add(int key)
	{
		LFNODE* pred, * curr;
		while (true) {
			FIND(pred, curr, key);

			if (key == curr->key) {
				return false;
			}
			else {
				LFNODE* node = new LFNODE(key);
				node->next.store(curr, false);
				if (true == pred->next.CAS(curr, node, false, false))
					return true;
			}
		}
	}

	bool Remove(int key)
	{
		LFNODE* pred, * curr;
		while (true) {
			FIND(pred, curr, key);

			if (key != curr->key) {
				return false;
			}
			else {
				LFNODE* succ = curr->next.get_addr();
				if (false == curr->next.CAS(succ, succ, false, true))
					continue;
				pred->next.CAS(curr, succ, false, false);
				return true;
			}
		}
	}


	bool Contains(int key)
	{
		LFNODE* curr = head.next.get_addr();
		while (curr->key < key)
			curr = curr->next.get_addr();
		return (curr->key == key) && (false == curr->next.is_removed());
	}

	void print_first_20()
	{
		LFNODE* curr = head.next.get_addr();
		for (int i = 0; i < 20; ++i) {
			if (&tail == curr) break;
			cout << curr->key << ", ";
			curr = curr->next.get_addr();
		}
		cout << endl;
	}

};

constexpr int MAX_LEVEL = 8;

class SK_NODE {
public:
	int key;
	SK_NODE* volatile next[MAX_LEVEL + 1];
	int top_level;
	volatile bool is_removed;
	volatile bool is_link_finished;
	recursive_mutex n_lock;

	SK_NODE()
	{
		key = 0;
		for (auto& n : next) n = nullptr;
		top_level = 0;
		is_removed = false;
	}
	SK_NODE(int x, int top)
	{
		key = x;
		for (auto& n : next) n = nullptr;
		top_level = top;
		is_removed = false;
		is_link_finished = false;
	}
};

class CSKLIST {  // 2020년 화목반 B조
	SK_NODE head, tail;
	mutex glock;
public:
	CSKLIST()
	{
		head.key = 0x80000000;
		tail.key = 0x7FFFFFFF;
		for (auto& n : head.next) n = &tail;
		head.top_level = tail.top_level = MAX_LEVEL;
	}
	~CSKLIST()
	{
		Init();
	}

	void Init()
	{
		SK_NODE* ptr;
		while (head.next[0] != &tail) {
			ptr = head.next[0];
			head.next[0] = head.next[0]->next[0];
			delete ptr;
		}
		for (auto& n : head.next) n = &tail;
	}

	void Find(int x, SK_NODE* preds[], SK_NODE* currs[])
	{
		preds[MAX_LEVEL] = &head;
		for (int cl = MAX_LEVEL; cl >= 0; --cl) {
			if (cl != MAX_LEVEL) preds[cl] = preds[cl + 1];
			currs[cl] = preds[cl]->next[cl];
			while (currs[cl]->key < x) {
				preds[cl] = currs[cl];
				currs[cl] = currs[cl]->next[cl];
			}
		}
	}

	bool Add(int key)
	{
		SK_NODE* preds[MAX_LEVEL + 1];
		SK_NODE* currs[MAX_LEVEL + 1];

		glock.lock();

		Find(key, preds, currs);

		if (key == currs[0]->key) {
			glock.unlock();
			return false;
		}
		else {
			int level = 0;		// 0레벨부터 시작하여 50% 확률로 최대레벨을 늘려서 레벨분포 구현
			while ((rand() % 2) == 0) {
				level++;
				if (MAX_LEVEL == level) break;
			}

			// 추가하기
			SK_NODE* node = new SK_NODE(key, level);
			for (int i = 0; i <= level; ++i) {
				node->next[i] = currs[i];
				preds[i]->next[i] = node;
			}

			glock.unlock();
			return true;
		}
	}

	bool Remove(int key)
	{
		SK_NODE* preds[MAX_LEVEL + 1];
		SK_NODE* currs[MAX_LEVEL + 1];

		glock.lock();
		Find(key, preds, currs);

		if (key != currs[0]->key) {
			glock.unlock();
			return false;
		}
		else {
			for (int i = 0; i <= currs[0]->top_level; ++i) {
				preds[i]->next[i] = currs[0]->next[i];
			}
			delete currs[0];

			glock.unlock();
			return true;
		}
	}

	bool Contains(int key)
	{
		SK_NODE* preds[MAX_LEVEL + 1];
		SK_NODE* currs[MAX_LEVEL + 1];

		glock.lock();

		Find(key, preds, currs);

		if (key != currs[0]->key) {
			glock.unlock();
			return false;
		}
		else {
			glock.unlock();
			return true;
		}
	}

	void print_first_20()
	{
		SK_NODE* curr = head.next[0];
		for (int i = 0; i < 20; ++i) {
			if (&tail == curr) break;
			cout << curr->key << ", ";
			curr = curr->next[0];
		}
		cout << endl;
	}

};

class LSKLIST {
	SK_NODE head, tail;
public:
	LSKLIST()
	{
		head.key = 0x80000000;
		tail.key = 0x7FFFFFFF;
		for (auto& n : head.next) n = &tail;
		head.top_level = tail.top_level = MAX_LEVEL;
		head.is_link_finished = tail.is_link_finished = true;
	}
	~LSKLIST()
	{
		Init();
	}

	void Init()
	{
		SK_NODE* ptr;
		while (head.next[0] != &tail) {
			ptr = head.next[0];
			head.next[0] = head.next[0]->next[0];
			delete ptr;
		}
		for (auto& n : head.next) n = &tail;
	}

	int Find(int x, SK_NODE* preds[], SK_NODE* currs[])
	{
		int l_found = -1;
		preds[MAX_LEVEL] = &head;
		for (int cl = MAX_LEVEL; cl >= 0; --cl) {
			if (cl != MAX_LEVEL) preds[cl] = preds[cl + 1];
			currs[cl] = preds[cl]->next[cl];

			if ((-1 == l_found) && (currs[cl]->key == x))
				l_found = cl;

			while (currs[cl]->key < x) {
				preds[cl] = currs[cl];
				currs[cl] = currs[cl]->next[cl];
			}
		}
		return l_found;
	}

	bool Add(int key)
	{
		int toplevel = 0;
		while ((rand() % 2) == 0) {
			toplevel++;
			if (MAX_LEVEL == toplevel) break;
		}

		SK_NODE* preds[MAX_LEVEL + 1];
		SK_NODE* currs[MAX_LEVEL + 1];

		while (true)
		{
			int lFound = Find(key, preds, currs);
			if (-1 != lFound)
			{
				SK_NODE* nodeFound = currs[lFound];
				if (!nodeFound->is_removed)
				{
					while (!nodeFound->is_link_finished) {}
					return false;
				}
				continue;
			}

			int highestLocked = -1;
			SK_NODE* pred;
			SK_NODE* curr;
			bool valid = true;
			for (int level = 0; valid && (level <= toplevel); ++level)
			{
				pred = preds[level];
				curr = currs[level];
				pred->n_lock.lock();
				highestLocked = level;
				valid = !pred->is_removed && !curr->is_removed && pred->next[level] == curr;
			}

			if (!valid)
			{
				for (int i = 0; i <= highestLocked; ++i)
					preds[i]->n_lock.unlock();
				continue;
			}

			SK_NODE* node = new SK_NODE(key, toplevel);
			for (int i = 0; i <= toplevel; ++i) {
				node->next[i] = currs[i];
			}
			for (int i = 0; i <= toplevel; ++i) {
				preds[i]->next[i] = node;
			}
			node->is_link_finished = true;

			for (int i = 0; i <= highestLocked; ++i)
				preds[i]->n_lock.unlock();
			return true;
		}
	}

	bool Remove(int key)
	{
		/*SK_NODE* preds[MAX_LEVEL + 1];
		SK_NODE* currs[MAX_LEVEL + 1];
		SK_NODE* victim = nullptr;
		bool isMarked = false;
		int topLevel = -1;

		while (true)
		{
			int lFound = Find(key, preds, currs);
			if (lFound != -1)
				victim = currs[lFound];
			if (isMarked | (lFound != -1 && (victim->is_link_finished && victim->top_level == lFound && !victim->is_removed)))
			{
				if (!isMarked)
				{
					topLevel = victim->top_level;
					victim->n_lock.lock();
					if (victim->is_removed)
					{
						victim->n_lock.unlock();
						return false;
					}
					victim->is_removed = true;
					isMarked = true;
				}

				int highestLocked = -1;
				SK_NODE* pred, * curr;
				bool valid = true;
				for (int level = 0; valid && (level <= topLevel); ++level)
				{
					pred = preds[level];
					pred->n_lock.lock();
					highestLocked = level;
					valid = !pred->is_removed && pred->next[level] == victim;
				}

				if (!valid)
				{
					for (int i = 0; i <= highestLocked; ++i)
						preds[i]->n_lock.unlock();
					continue;
				}

				for (int level = topLevel; level >= 0; --level)
				{
					preds[level]->next[level] = victim->next[level];
				}
				victim->n_lock.unlock();
				for (int i = 0; i <= highestLocked; ++i)
					preds[i]->n_lock.unlock();
				return true;
			}
			else
			{
				return false;
			}
		}*/

		SK_NODE* preds[MAX_LEVEL + 1];
		SK_NODE* currs[MAX_LEVEL + 1];

		int f_level = Find(key, preds, currs);
		if (-1 == f_level)	return false;
		if (true == currs[0]->is_removed) return false;
		if (false == currs[0]->is_link_finished) return false;
		if (f_level != currs[0]->top_level) return false;

		currs[0]->n_lock.lock();
		if (true == currs[0]->is_removed)
		{
			currs[0]->n_lock.unlock();
			return false;
		}
		currs[0]->is_removed = true;

		while (true)
		{
			bool is_valid = true;
			int cl;
			for (cl = 0; cl <= MAX_LEVEL; ++cl)
			{
				preds[cl]->n_lock.lock();
				is_valid = (false == preds[cl]->is_removed) && (currs[cl] == preds[cl]->next[cl]);
				if (false == is_valid)
					break;
			}

			if (false == is_valid)
			{
				for (int i = 0; i <= cl; ++i)
					preds[i]->n_lock.unlock();
				int f_level = Find(key, preds, currs);
				continue;
			}

			for (int i = currs[0]->top_level; i >= 0; --i)
				preds[i]->next[i] = currs[i]->next[i];

			for (int i = 0; i <= MAX_LEVEL; ++i)
				preds[i]->n_lock.unlock();

			currs[0]->n_lock.unlock();
			return true;
		}
	}

	bool Contains(int key)
	{
		SK_NODE* preds[MAX_LEVEL + 1];
		SK_NODE* currs[MAX_LEVEL + 1];

		int f_level = Find(key, preds, currs);

		return ((f_level != -1) && (true == currs[f_level]->is_link_finished) && (false == currs[f_level]->is_removed));
	}

	void print_first_20()
	{
		SK_NODE* curr = head.next[0];
		for (int i = 0; i < 20; ++i) {
			if (&tail == curr) break;
			cout << curr->key << ", ";
			curr = curr->next[0];
		}
		cout << endl;
	}

};

LSKLIST clist;

void ThreadFunc(int num_thread, int t_id)
{
	int key;

	thread_id = t_id;

	for (int i = 0; i < NUM_TEST / num_thread; i++) {
		switch (rand() % 3) {
		case 0: key = rand() % KEY_RANGE;
			clist.Add(key);
			break;
		case 1: key = rand() % KEY_RANGE;
			clist.Remove(key);
			break;
		case 2: key = rand() % KEY_RANGE;
			clist.Contains(key);
			break;
		default: cout << "Error\n";
			exit(-1);
		}
	}
}

int main()
{
	for (int num = 1; num <= MAX_THREAD; num = num * 2) {
		vector <thread> threads;
		clist.Init();
		auto start_t = high_resolution_clock::now();
		for (int i = 0; i < num; ++i)
			threads.emplace_back(ThreadFunc, num, i);
		for (auto& th : threads) th.join();
		auto end_t = high_resolution_clock::now();
		auto du = end_t - start_t;
		// 벤치마크 프로그램 실행
		clist.print_first_20();
		cout << "Number of threads = " << num;
		cout << ",  Exec Time = " << duration_cast<milliseconds>(du).count() << "ms";
		cout << endl;
	}
}
