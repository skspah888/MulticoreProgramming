#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <chrono>
#include <atomic>
#include <set>
using namespace std;
using namespace chrono;

constexpr int NUM_TEST = 4000000;
constexpr int MAX_THREAD = 8;
constexpr int KEY_RANGE = 1000;

///// 무잠금 만능
///// STL_SET : 싱글쓰레드

class LFNODE;

class CPTR		// 합성 자료구조
{
	atomic_int ptr;

public:
	CPTR()
	{
		ptr = 0;
	}
	CPTR(LFNODE* addr, bool marking)
	{
		int next = reinterpret_cast<int>(addr);		// addr을 int로 변환
		if (true == marking)
			next = next | 0x01;						// int 주소중 한 비트를 marking 으로 사용함

		ptr = next;
	}

	void store(LFNODE* addr, bool marking)
	{
		int next = reinterpret_cast<int>(addr);		// addr을 int로 변환
		if (true == marking)
			next = next | 0x01;						// int 주소중 한 비트를 marking 으로 사용함

		ptr = next;
	}


	~CPTR() {}

	LFNODE* get_addr()
	{
		return reinterpret_cast<LFNODE*>(ptr & 0xFFFFFFFE);
		//return reinterpret_cast<LFNODE*>(next & 0xFFFFFFFFFFFFFFFE);	 64비트일때
	}

	LFNODE* get_addr(bool* marking)		// 주소와 마킹을 동시에 전달
	{
		int next = ptr;
		*marking = (0x01 == (next & 0x01));
		return reinterpret_cast<LFNODE*>(next & 0xFFFFFFFE);
	}

	bool CAS(LFNODE* old_addr, LFNODE* new_addr, bool old_mark, bool new_mark)
	{
		int old_next = reinterpret_cast<int>(old_addr);
		if (true == old_mark) old_next++;

		int new_next = reinterpret_cast<int>(new_addr);
		if (true == new_mark) new_next++;

		return atomic_compare_exchange_strong(&ptr, &old_next, new_next);
	}

};

class LFNODE
{
public:
	int key;
	CPTR next;

	LFNODE() {}

	LFNODE(int key_value)
	{
		key = key_value;
	}
	~LFNODE() {}
};

class STL_SET
{
	set<int> m_set;
public:
	STL_SET()
	{
	}
	~STL_SET() {}

	void Init()
	{
		m_set.clear();
	}

	bool Add(int key)
	{
		// 이미 있으면 false, 없으면 추가
		if (0 != m_set.count(key))
			return false;

		m_set.insert(key);
		return true;
	}

	bool Remove(int key)
	{
		// 없으면 false, 있으면 삭제
		if (0 == m_set.count(key))
			return false;

		m_set.erase(key);
		return true;
	}

	bool Contains(int key)
	{
		return 0 != m_set.count(key);
	}

	void print_first_20()
	{
		int count = 0;
		for (auto num : m_set) {
			if (count++ > 20) break;
			cout << num << ", ";
		}
		cout << endl;
	}
};


STL_SET flist;

void ThreadFunc(int num_thread) {
	int key;
	for (int i = 0; i < NUM_TEST / num_thread; i++) {
		switch (rand() % 3)
		{
		case 0:
			key = rand() % KEY_RANGE;
			flist.Add(key);
			break;

		case 1:
			key = rand() % KEY_RANGE;
			flist.Remove(key);
			break;

		case 2:
			key = rand() % KEY_RANGE;
			flist.Contains(key);
			break;

		default:
			cout << "Error\n";
			exit(-1);
		}
	}
}

int main()
{
	for (int num = 1; num <= MAX_THREAD; num = num * 2)
	{
		flist.Init();

		// 벤치마크 프로그램 실행
		vector<thread> workers;

		auto start_t = high_resolution_clock::now();

		for (int i = 0; i < num; ++i)
			workers.emplace_back(ThreadFunc, num);

		for (auto& th : workers)
			th.join();

		auto end_t = high_resolution_clock::now();
		auto exec_time = end_t - start_t;

		cout << "Number of threads = " << num;
		cout << ",  Exec Time = " << duration_cast<milliseconds>(exec_time).count() << "ms" << endl;
		flist.print_first_20();

		cout << endl;
	}
}