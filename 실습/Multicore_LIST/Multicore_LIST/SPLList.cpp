#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <chrono>
using namespace std;
using namespace chrono;

constexpr int NUM_TEST = 4000000;
constexpr int MAX_THREAD = 8;
constexpr int KEY_RANGE = 1000;

// 게으른 동기화 + Shared_ptr 사용
// 실습 24

class NODE
{
public:
	int key;
	volatile bool marked = false;
	NODE* next;
	mutex n_lock;

	NODE()
	{
		next = NULL;
	}
	NODE(int key_value)
	{
		next = NULL;
		key = key_value;
	}
	~NODE() {}

	void lock()
	{
		n_lock.lock();
	}

	void unlock()
	{
		n_lock.unlock();
	}

};

class SPNODE
{
public:
	int key;
	shared_ptr<SPNODE> next;
	bool marked;
	mutex n_lock;

	SPNODE()
	{
		next = NULL;
		marked = false;
	}
	SPNODE(int key_value)
	{
		next = NULL;
		key = key_value;
		marked = false;
	}
	~SPNODE() {}

	void lock()
	{
		n_lock.lock();
	}

	void unlock()
	{
		n_lock.unlock();
	}
};

class SPLLIST
{
	shared_ptr<SPNODE> head;
	shared_ptr<SPNODE> tail;

public:
	SPLLIST()
	{
		head = make_shared<SPNODE>(0x80000000);
		tail= make_shared<SPNODE>(0x7FFFFFFF);
		head->next = tail;
	}
	~SPLLIST() {}

	void Init()
	{
		// 알아서 다 날아감
		head->next = tail;
	}

	bool is_valid(const shared_ptr<SPNODE>& pred, const shared_ptr<SPNODE>& curr)
	{
		return (!pred->marked) && (!curr->marked) && (pred->next == curr);
	}

	bool Add(int key)
	{
		while (true)
		{
			shared_ptr<SPNODE> pred, curr;
			pred = head;
			curr = pred->next;

			while (curr->key < key)
			{
				pred = curr;
				curr = curr->next;
			}

			// 수정하기전 Lock걸기
			pred->lock();
			curr->lock();

			if (is_valid(pred, curr))
			{
				if (key == curr->key)
				{
					curr->unlock();
					pred->unlock();
					return false;
				}

				else
				{
					shared_ptr<SPNODE> node = make_shared<SPNODE>(key);
					node->next = curr;
					pred->next = node;

					pred->unlock();
					curr->unlock();

					return true;
				}
			}
			else
			{
				curr->unlock();
				pred->unlock();
			}
		}
	}
	bool Remove(int key)
	{
		while (true)
		{
			shared_ptr<SPNODE> pred, curr;
			pred = head;
			curr = pred->next;

			while (curr->key < key)
			{
				pred = curr;
				curr = curr->next;
			}

			// 수정하기전 Lock걸기
			pred->lock();
			curr->lock();

			if (is_valid(pred, curr))
			{
				if (key != curr->key)
				{
					curr->unlock();
					pred->unlock();
					return false;
				}
				else
				{
					curr->marked = true;
					pred->next = curr->next;
					curr->unlock();
					pred->unlock();
					return true;
				}
			}

			else
			{
				curr->unlock();
				pred->unlock();
			}
		}
	}

	bool Contains(int key)
	{
		shared_ptr<SPNODE> curr = head->next;
		while (curr->key < key)
			curr = curr->next;

		return curr->key == key && !curr->marked;
	}

	void print_first_20()
	{
		shared_ptr<SPNODE> curr = head->next;
		for (int i = 0; i < 20; ++i)
		{
			if (tail == curr)
				break;
			cout << curr->key << ", ";
			curr = curr->next;
		}
		cout << endl;
	}
};

SPLLIST flist;

void ThreadFunc(int num_thread) {
	int key;
	for (int i = 0; i < NUM_TEST / num_thread; i++) {
		switch (rand() % 3)
		{
		case 0:
			key = rand() % KEY_RANGE;
			flist.Add(key);
			break;

		case 1:
			key = rand() % KEY_RANGE;
			flist.Remove(key);
			break;

		case 2:
			key = rand() % KEY_RANGE;
			flist.Contains(key);
			break;

		default:
			cout << "Error\n";
			exit(-1);
		}
	}
}

int main()
{
	for (int num = 1; num <= MAX_THREAD; num = num * 2)
	{
		flist.Init();

		// 벤치마크 프로그램 실행
		vector<thread> workers;

		auto start_t = high_resolution_clock::now();

		for (int i = 0; i < num; ++i)
			workers.emplace_back(ThreadFunc, num);

		for (auto& th : workers)
			th.join();

		auto end_t = high_resolution_clock::now();
		auto exec_time = end_t - start_t;

		cout << "Number of threads = " << num;
		cout << ",  Exec Time = " << duration_cast<milliseconds>(exec_time).count() << "ms" << endl;
		flist.print_first_20();

		cout << endl;
	}
}