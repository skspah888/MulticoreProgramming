#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <chrono>
#include <atomic>
using namespace std;
using namespace chrono;

constexpr int NUM_TEST = 4000000;
constexpr int MAX_THREAD = 8;
constexpr int KEY_RANGE = 1000;

///// 비멈춤 동기화

class LFNODE;

class CPTR		// 합성 자료구조
{
	atomic_int ptr;

public:
	CPTR() 
	{
		ptr = 0;
	}
	CPTR(LFNODE* addr, bool marking)
	{
		int next = reinterpret_cast<int>(addr);		// addr을 int로 변환
		if (true == marking)
			next = next | 0x01;						// int 주소중 한 비트를 marking 으로 사용함

		ptr = next;
	}

	void store(LFNODE* addr, bool marking)
	{
		int next = reinterpret_cast<int>(addr);		// addr을 int로 변환
		if (true == marking)
			next = next | 0x01;						// int 주소중 한 비트를 marking 으로 사용함

		ptr = next;
	}


	~CPTR() {}

	LFNODE* get_addr()
	{
		return reinterpret_cast<LFNODE*>(ptr & 0xFFFFFFFE);
		//return reinterpret_cast<LFNODE*>(next & 0xFFFFFFFFFFFFFFFE);	 64비트일때
	}

	LFNODE* get_addr(bool* marking)		// 주소와 마킹을 동시에 전달
	{
		int next = ptr;
		*marking = (0x01 == (next & 0x01));
		return reinterpret_cast<LFNODE*>(next & 0xFFFFFFFE);
	}

	bool CAS(LFNODE* old_addr, LFNODE* new_addr, bool old_mark, bool new_mark)
	{
		int old_next = reinterpret_cast<int>(old_addr);
		if (true == old_mark) old_next++;

		int new_next = reinterpret_cast<int>(new_addr);
		if (true == new_mark) new_next++;
		
		return atomic_compare_exchange_strong(&ptr, &old_next, new_next);
	}

};

class LFNODE
{
public:
	int key;
	CPTR next;

	LFNODE() {}

	LFNODE(int key_value)
	{
		key = key_value;
	}
	~LFNODE() {}
};

class LFLIST
{
	LFNODE head, tail;
public:
	LFLIST()
	{
		head.key = 0x80000000;
		tail.key = 0x7FFFFFFF;
		head.next.store(&tail,false);
	}
	~LFLIST() {}

	void Init()
	{
		LFNODE* ptr;
		while (head.next.get_addr() != &tail)
		{
			ptr = head.next.get_addr();
			head.next.store(ptr->next.get_addr(), false);
			delete ptr;
		}
	}

	// 그냥 검색을 하는것이 아니라 제거하면서 검색
	void Find(LFNODE* &pred, LFNODE* &curr, int key)
	{
	retry:
		pred = &head;
		curr = pred->next.get_addr();

		// curr이 지워진 노드일수 있으니 함부로 사용할수 없음
		while (true)
		{
			// marking된 curr 지우기
			bool is_removed;
			LFNODE* succ = curr->next.get_addr(&is_removed);
			while (true == is_removed)
			{
				if (true == pred->next.CAS(curr, succ, false, false))
				{
					curr = succ;
					succ = curr->next.get_addr(&is_removed);
				}
				else
					goto retry;
			}

			if (curr->key >= key)
				return;

			pred = curr;
			curr = succ;
		}
	}

	bool Add(int key)
	{
		LFNODE* pred, *curr;
		while (true)
		{
			Find(pred, curr, key);

			if (key == curr->key)
				return false;
			else
			{
				LFNODE* node = new LFNODE(key);
				node->next.store(curr, false);
				if(true == pred->next.CAS(curr, node, false, false))
					return true;
			}
		}
	}

	bool Remove(int key)
	{
		LFNODE* pred, *curr;
		bool snip;
		while (true)
		{
			Find(pred, curr, key);

			if (curr->key != key)
				return false;
			else
			{
				LFNODE* succ = curr->next.get_addr();

				snip = curr->next.CAS(pred, succ, false, false);

				if (!snip)
					continue;

				pred->next.CAS(curr, succ, false, false);
				return true;
			}

		}
	}

	bool Contains(int key)
	{
		LFNODE* curr = head.next.get_addr();
		bool marked[] = { false, };

		while (curr->key < key)
		{
			curr = curr->next.get_addr();
			LFNODE* succ = curr->next.get_addr(marked);
		}

		return curr->key == key && !marked[0];
	}

	void print_first_20()
	{
		LFNODE* curr = head.next.get_addr();
		for (int i = 0; i < 20; ++i)
		{
			if (&tail == curr)
				break;
			cout << curr->key << ", ";
			curr = curr->next.get_addr();
		}
		cout << endl;
	}
};

LFLIST flist;

void ThreadFunc(int num_thread) {
	int key;
	for (int i = 0; i < NUM_TEST / num_thread; i++) {
		switch (rand() % 3)
		{
		case 0:
			key = rand() % KEY_RANGE;
			flist.Add(key);
			break;

		case 1:
			key = rand() % KEY_RANGE;
			flist.Remove(key);
			break;

		case 2:
			key = rand() % KEY_RANGE;
			flist.Contains(key);
			break;

		default:
			cout << "Error\n";
			exit(-1);
		}
	}
}

int main()
{
	for (int num = 1; num <= MAX_THREAD; num = num * 2)
	{
		flist.Init();

		// 벤치마크 프로그램 실행
		vector<thread> workers;

		auto start_t = high_resolution_clock::now();

		for (int i = 0; i < num; ++i)
			workers.emplace_back(ThreadFunc, num);

		for (auto& th : workers)
			th.join();

		auto end_t = high_resolution_clock::now();
		auto exec_time = end_t - start_t;

		cout << "Number of threads = " << num;
		cout << ",  Exec Time = " << duration_cast<milliseconds>(exec_time).count() << "ms" << endl;
		flist.print_first_20();

		cout << endl;
	}
}