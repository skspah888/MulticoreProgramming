#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <chrono>
#include <atomic>

using namespace std;
using namespace chrono;

/// <summary>
/// 과제7 : 비멈춤 동기화
/// </summary>

constexpr int NUM_TEST = 4000000;
constexpr int MAX_THREAD = 8;
constexpr int KEY_RANGE = 1000;

class LFNODE;

class CPTR
{
	atomic_int ptr;
public:
	CPTR() {
		ptr = 0;
	}
	CPTR(LFNODE* addr, bool marking)
	{
		// addr을 int로 변환
		int next = reinterpret_cast<int>(addr);

		// int 주소중 한 비트를 marking으로 사용
		if (true == marking)
			next = next | 0x01;
		ptr = next;
	}

	void store(LFNODE* addr, bool marking)
	{
		int next = reinterpret_cast<int>(addr);
		if (true == marking)
			next = next | 0x01;
		ptr = next;
	}

	~CPTR() {}

	LFNODE* get_addr()
	{
		return reinterpret_cast<LFNODE*>(ptr & 0xFFFFFFFE);
		//return reinterpret_cast<LFNODE*>(ptr & 0xFFFFFFFFFFFFFFFE);	// 64비트일때
	}

	bool is_removed()
	{
		return 1 == (ptr & 0x01);
	}

	LFNODE* get_addr(bool* marking)
	{
		int next = ptr;
		*marking = (0x01 == (next & 0x01));
		return reinterpret_cast<LFNODE*>(next & 0xFFFFFFFE);
		//return reinterpret_cast<LFNODE*>(next & 0xFFFFFFFFFFFFFFFE);
	}

	bool CAS(LFNODE* old_addr, LFNODE* new_addr, bool old_mark, bool new_mark)
	{
		int old_next = reinterpret_cast<int>(old_addr);
		if (true == old_mark) old_next++;

		int new_next = reinterpret_cast<int>(new_addr);
		if (true == new_mark) new_next++;

		return atomic_compare_exchange_strong(&ptr, &old_next, new_next);
	}

	bool AttemptMark(LFNODE* old_node, bool newMark)
	{
		int oldvalue = reinterpret_cast<int>(old_node);
		int newvalue = oldvalue;
		if (newMark)
			newvalue = newvalue | 0x01;
		else
			newvalue = newvalue & 0xFFFFFFFE;
		
		return atomic_compare_exchange_strong(&ptr, &oldvalue, newvalue);
	}

};

class LFNODE {
public:
	int key;
	CPTR next;

	LFNODE() { }

	LFNODE(int key_value) {
		key = key_value;
	}

	~LFNODE() {}
};

class LFLIST {
	LFNODE head, tail;
public:
	LFLIST()
	{
		head.key = 0x80000000;
		tail.key = 0x7FFFFFFF;
		head.next.store(&tail, false);
	}
	~LFLIST() {}

	void Init()
	{
		LFNODE* ptr;
		while (head.next.get_addr() != &tail) {
			ptr = head.next.get_addr();
			head.next.store(ptr->next.get_addr(), false);
			delete ptr;
		}
	}

	// 제거하면서 검색
	void FIND(LFNODE*& pred, LFNODE*& curr, int key)
	{
	retry:
		pred = &head;
		curr = pred->next.get_addr();

		while (true) {
			// marking된 curr지우기
			bool is_removed;
			LFNODE* succ = curr->next.get_addr(&is_removed);
			while (true == is_removed)
				if (true == pred->next.CAS(curr, succ, false, false)) {
					curr = succ;
					succ = curr->next.get_addr(&is_removed);
				}
				else
					goto retry;

			if (curr->key >= key) return;
			pred = curr;
			curr = succ;
		}
	}

	bool Add(int key)
	{
		LFNODE* pred, * curr;

		while (true)
		{
			FIND(pred, curr, key);

			if (key == curr->key)
				return false;
			else
			{
				LFNODE* node = new LFNODE(key);
				node->next.store(curr, false);

				if (true == pred->next.CAS(curr, node, false, false))
					return true;
			}
		}
	}

	bool Remove(int key)
	{
		LFNODE* pred, * curr;
		bool snip;
		while (true)
		{
			FIND(pred, curr, key);

			if (curr->key != key)
				return false;
			else
			{
				LFNODE* succ = curr->next.get_addr();
				
				//snip = curr->next.CAS(succ, curr, true, true);
				snip = curr->next.AttemptMark(succ, true);

				if (!snip)
					continue;

				pred->next.CAS(curr, succ, false, false);
				return true;
			}
		}
	}

	bool Contains(int key)
	{
		LFNODE* curr = head.next.get_addr();
		bool is_removed = false;

		while (curr->key < key)
		{
			curr = curr->next.get_addr();
			LFNODE* succ = curr->next.get_addr(&is_removed);
		}
		return curr->key == key && !is_removed;
	}

	void print_first_20()
	{
		LFNODE* curr = head.next.get_addr();
		for (int i = 0; i < 20; ++i) {
			if (&tail == curr) break;
			cout << curr->key << ", ";
			curr = curr->next.get_addr();
		}
		cout << endl;
	}

};

LFLIST clist;

void ThreadFunc(int num_thread)
{
	int key;

	for (int i = 0; i < NUM_TEST / num_thread; i++) {
		switch (rand() % 3) {
		case 0: key = rand() % KEY_RANGE;
			clist.Add(key);
			break;
		case 1: key = rand() % KEY_RANGE;
			clist.Remove(key);
			break;
		case 2: key = rand() % KEY_RANGE;
			clist.Contains(key);
			break;
		default: cout << "Error\n";
			exit(-1);
		}
	}
}

int main()
{
	for (int num = 1; num <= MAX_THREAD; num = num * 2) {
		clist.Init();
		vector <thread> threads;
		auto start_t = high_resolution_clock::now();
		for (int i = 0; i < num; ++i)
			threads.emplace_back(ThreadFunc, num);
		for (auto& th : threads) th.join();
		auto end_t = high_resolution_clock::now();
		auto du = end_t - start_t;
		// 벤치마크 프로그램 실행
		clist.print_first_20();
		cout << "Number of threads = " << num;
		cout << ",  Exec Time = " << duration_cast<milliseconds>(du).count() << "ms";
		cout << endl;
	}
}
