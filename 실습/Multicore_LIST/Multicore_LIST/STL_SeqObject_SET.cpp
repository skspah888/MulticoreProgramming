#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <chrono>
#include <atomic>
#include <set>
using namespace std;
using namespace chrono;

constexpr int NUM_TEST = 4000000;
constexpr int MAX_THREAD = 8;
constexpr int KEY_RANGE = 1000;

///// 무잠금 만능
///// STL_SET : SeqObject

constexpr int ADD = 0;
constexpr int REMOVE = 1;
constexpr int CONTAINS = 2;
constexpr int CLEAR = 3;
constexpr int DISPLAY20 = 4;

class Invocation
{
public:
	int method;		// m_set을 어떤 Method로 호출할지
	int para1;
};

struct Response
{
	bool ret1;
};

class SeqObject_SET
{
	set<int> m_set;

public:
	Response apply(const Invocation& inv)
	{
		switch (inv.method)
		{
		case ADD:
			if (0 != m_set.count(inv.para1))
				return Response{ false };

			m_set.insert(inv.para1);
			return Response{ true };

		case REMOVE:
			if (0 == m_set.count(inv.para1))
				return Response{ false };

			m_set.erase(inv.para1);
			return Response{ true };

		case CONTAINS:
			return Response{ 0 != m_set.count(inv.para1) };

		case CLEAR:
			m_set.clear();
			return Response();

		case DISPLAY20:
			int count = 0;
			for (auto num : m_set) {
				if (count++ > 20) break;
				cout << num << ", ";
			}
			cout << endl;
			return Response();
		}

		return Response();
	}
};

class SEQ_SET
{
	SeqObject_SET m_set;

public:
	SEQ_SET()
	{
	}
	~SEQ_SET() {}

	void Init()
	{
		m_set.apply(Invocation{ CLEAR, 0 });
	}

	bool Add(int key)
	{
		return m_set.apply(Invocation{ ADD, key }).ret1;
	}

	bool Remove(int key)
	{
		return m_set.apply(Invocation{ REMOVE, key }).ret1;
	}

	bool Contains(int key)
	{
		return m_set.apply(Invocation{ CONTAINS, key }).ret1;
	}

	void print_first_20()
	{
		m_set.apply(Invocation{ DISPLAY20, 0 });
	}
};

SEQ_SET flist;

void ThreadFunc(int num_thread) {
	int key;
	for (int i = 0; i < NUM_TEST / num_thread; i++) {
		switch (rand() % 3)
		{
		case 0:
			key = rand() % KEY_RANGE;
			flist.Add(key);
			break;

		case 1:
			key = rand() % KEY_RANGE;
			flist.Remove(key);
			break;

		case 2:
			key = rand() % KEY_RANGE;
			flist.Contains(key);
			break;

		default:
			cout << "Error\n";
			exit(-1);
		}
	}
}

int main()
{
	for (int num = 1; num <= MAX_THREAD; num = num * 2)
	{
		flist.Init();

		// 벤치마크 프로그램 실행
		vector<thread> workers;

		auto start_t = high_resolution_clock::now();

		for (int i = 0; i < num; ++i)
			workers.emplace_back(ThreadFunc, num);

		for (auto& th : workers)
			th.join();

		auto end_t = high_resolution_clock::now();
		auto exec_time = end_t - start_t;

		cout << "Number of threads = " << num;
		cout << ",  Exec Time = " << duration_cast<milliseconds>(exec_time).count() << "ms" << endl;
		flist.print_first_20();

		cout << endl;
	}
}