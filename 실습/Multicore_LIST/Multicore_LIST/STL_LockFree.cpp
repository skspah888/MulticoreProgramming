#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <chrono>
#include <atomic>
#include <set>
using namespace std;
using namespace chrono;

constexpr int NUM_TEST = 4000;
constexpr int MAX_THREAD = 16;
constexpr int KEY_RANGE = 1000;

///// 무잠금 만능
///// STL_SET : LFUN_SET [ LockFree ( LF ) , LFUniversal 사용 ]

constexpr int ADD = 0;
constexpr int REMOVE = 1;
constexpr int CONTAINS = 2;
constexpr int CLEAR = 3;
constexpr int DISPLAY20 = 4;

thread_local int thread_id;

class Invocation
{
public:
	int method;		// m_set을 어떤 Method로 호출할지
	int para1;
};

struct Response
{
	bool ret1;
};

class SeqObject_SET
{
	set<int> m_set;

public:
	Response apply(const Invocation& inv)
	{
		switch (inv.method)
		{
		case ADD:
			if (0 != m_set.count(inv.para1))
				return Response{ false };

			m_set.insert(inv.para1);
			return Response{ true };

		case REMOVE:
			if (0 == m_set.count(inv.para1))
				return Response{ false };

			m_set.erase(inv.para1);
			return Response{ true };

		case CONTAINS:
			return Response{ 0 != m_set.count(inv.para1) };

		case CLEAR:
			m_set.clear();
			return Response();

		case DISPLAY20:
			int count = 0;
			for (auto num : m_set) {
				if (count++ > 20) break;
				cout << num << ", ";
			}
			cout << endl;
			return Response();
		}

		return Response();
	}
};

class UNODE;

class ConsensusNext
{
	atomic_int cnext;
public:
	ConsensusNext()
	{
		cnext = 0;
	}

	UNODE* decide(UNODE* next)	// 여러 노드의 decide요청이 들어오지만, CAS를 하여 한개의 NODE를 고른다
	{
		int nv = reinterpret_cast<int>(next);
		int ov = 0;
		if (true == atomic_compare_exchange_strong(&cnext, &ov, nv))
			return next;
		return reinterpret_cast<UNODE*>(ov);		// cnext를 retrun해도 되지만 atomic_int여서 casting이 까다로움
	}

	void clear()
	{
		cnext = 0;
	}
};

class UNODE
{
public:
	int seq;
	UNODE* next;
	Invocation inv;
	ConsensusNext decidenext;	// Next값을 합의객체하기 위함

	UNODE(const Invocation& pinv)
	{
		inv = pinv;
		next = nullptr;
		seq = 0;
	}

	void clear()
	{
		next = nullptr;
		decidenext.clear();
	}
};

class LFUniversalSet
{
	UNODE* tail;
	UNODE* head[MAX_THREAD];
public:
	LFUniversalSet()
	{
		tail = new UNODE{ Invocation() };
		tail->seq = 1;
		for (auto& h : head)
			h = tail;
	}

	UNODE* get_last_node()
	{
		// 모든 노드가 가지고있는 노드중 최신 노드
		// 검색하는 동안 딴 쓰레드가 중간에 껴들경우 최신노드가 아닐 수 있음
		UNODE* p = head[0];
		for (auto h : head)
		{
			if (h->seq > p->seq)
				p = h;
		}
		return p;
	}

	void clear()
	{
		// log를 지우고, tail의 decide를 지운 후, head를 초기화한다.
		UNODE* p = tail->next;
		while (nullptr != p)
		{
			UNODE* q = p;
			p = p->next;
			delete q;
		}

		// decide 초기화
		tail->clear();

		// head 초기화
		for (auto& h : head)
			h = tail;
	}

	Response apply(const Invocation& inv)
	{
		UNODE* prefer = new UNODE{ inv };

		// tail에서 노드들이 쭉 연결되어있을때 맨 뒤의 노드에 연결해야한다
		while (0 == prefer->seq)
		{
			UNODE* before = get_last_node();
			// 딴 쓰레드가 먼저 했으면 내가 Lose ( 즉, 이길수도있고 질 수도있음 )
			UNODE* after = before->decidenext.decide(prefer);
			before->next = after;
			after->seq = before->seq + 1;
			head[thread_id] = after;
		}


		// 가장 먼저 최적화 해야할 부분
		SeqObject_SET l_set;
		UNODE* exec = tail->next;
		while (exec != prefer)		// prefer는 실행하면 안됨
		{
			l_set.apply(exec->inv);
			exec = exec->next;
		}
		return l_set.apply(inv);
	}
};

class LFUN_SET
{
	LFUniversalSet m_set;

public:
	LFUN_SET()
	{
	}
	~LFUN_SET() {}

	void Init()
	{
		m_set.clear();
		//m_set.apply(Invocation{ CLEAR, 0 });
	}

	bool Add(int key)
	{
		return m_set.apply(Invocation{ ADD, key }).ret1;
	}

	bool Remove(int key)
	{
		return m_set.apply(Invocation{ REMOVE, key }).ret1;
	}

	bool Contains(int key)
	{
		return m_set.apply(Invocation{ CONTAINS, key }).ret1;
	}

	void print_first_20()
	{
		m_set.apply(Invocation{ DISPLAY20, 0 });
	}
};

LFUN_SET flist;

void ThreadFunc(int num_thread, int t_id) {
	int key;

	thread_id = t_id;

	for (int i = 0; i < NUM_TEST / num_thread; i++) {
		switch (rand() % 3)
		{
		case 0:
			key = rand() % KEY_RANGE;
			flist.Add(key);
			break;

		case 1:
			key = rand() % KEY_RANGE;
			flist.Remove(key);
			break;

		case 2:
			key = rand() % KEY_RANGE;
			flist.Contains(key);
			break;

		default:
			cout << "Error\n";
			exit(-1);
		}
	}
}

int main()
{
	for (int num = 1; num <= MAX_THREAD; num = num * 2)
	{
		// 벤치마크 프로그램 실행
		vector<thread> workers;
		flist.Init();

		auto start_t = high_resolution_clock::now();

		for (int i = 0; i < num; ++i)
			workers.emplace_back(ThreadFunc, num, i);

		for (auto& th : workers)
			th.join();

		auto end_t = high_resolution_clock::now();
		auto exec_time = end_t - start_t;

		cout << "Number of threads = " << num;
		cout << ",  Exec Time = " << duration_cast<milliseconds>(exec_time).count() << "ms" << endl;
		flist.print_first_20();

		cout << endl;
	}
}