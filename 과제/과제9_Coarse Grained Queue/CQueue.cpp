#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <chrono>
using namespace std;
using namespace chrono;

constexpr int NUM_TEST = 10000000;
constexpr int MAX_THREAD = 8;

///// 성긴 동기화 큐
///// Coarse Grain Queue

class NODE
{
public:
	int key;
	NODE* next;

	NODE()
	{
		key = 0;
		next = NULL;
	}
	NODE(int key_value)
	{
		key = key_value;
		next = nullptr;
	}
	~NODE() {}
};

class null_mutex
{
public:
	void lock() {}
	void unlock() {}
};

class CQUEUE
{
	NODE *head, *tail;
	mutex glock;

public:
	CQUEUE()
	{
		head = tail = new NODE();
	}
	~CQUEUE() 
	{
		Init();
		delete head;
	};

	void Init()
	{
		NODE* ptr;
		while (head != tail) {
			ptr = head;
			head = ptr->next;
			delete ptr;
		}
	}

	void Enq(int key)
	{
		NODE* e = new NODE{ key };
		glock.lock();
		tail->next = e;
		tail = e;
		glock.unlock();
	}

	int Deq()
	{
		glock.lock();
		if (head == tail)
		{
			glock.unlock();
			return -1;
		}

		NODE* ptr = head;
		head = head->next;
		glock.unlock();
		delete ptr;
		return head->key;
	}

	void print_first_20()
	{
		NODE* curr = head->next;
		for (int i = 0; i < 20; ++i) {
			if (nullptr == curr) break;

			cout << curr->key << ", ";
			curr = curr->next;
		}
		cout << endl;
	}
};

CQUEUE my_queue;

void worker(int num_thread) {
	

	for (int i = 0; i < NUM_TEST / num_thread; i++) {
		switch (rand() % 2)
		{
		case 0:
			my_queue.Enq(i);
			break;

		case 1:
			my_queue.Deq();
			break;

		default:
			cout << "Error\n";
			exit(-1);
		}
	}
}

int main()
{
	for (int num = 1; num <= MAX_THREAD; num = num * 2)
	{
		my_queue.Init();

		// 벤치마크 프로그램 실행
		vector<thread> workers;

		auto start_t = high_resolution_clock::now();

		for (int i = 0; i < num; ++i)
			workers.emplace_back(worker, num);

		for (auto& th : workers)
			th.join();

		auto end_t = high_resolution_clock::now();
		auto exec_time = end_t - start_t;

		cout << "Number of threads = " << num;
		cout << ",  Exec Time = " << duration_cast<milliseconds>(exec_time).count() << "ms" << endl;
		my_queue.print_first_20();
		cout << endl;
	}
}