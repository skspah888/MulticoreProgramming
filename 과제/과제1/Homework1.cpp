#include <iostream>
#include <thread>
#include <vector>
#include <mutex>
#include <chrono>
#include <atomic>
using namespace std;
using namespace chrono;

const int MAX_TRHEAD = 8;
const int MAX_NUMBER = 100000000;

volatile int SUM = 0;

mutex sum_lock;

volatile bool choosing[100]; 
volatile int number[100] = { 0 };

int MAX(int count)	
{
	int MAXNUM = 0;
	for (int i = 0; i < count; ++i)
	{
		for (int j = 0; j < count; ++j)
		{
			if (number[i] > number[j])
			{
				MAXNUM = number[j];
			}
		}
	}

	return MAXNUM;
}


void Bread_lock(int i, int count)
{
	choosing[i] = true; 
	number[i] = 1 + MAX(count);
	choosing[i] = false; 

	for (int j = 0; j < count; ++j) 
	{
		while (choosing[j]) {} 
		while ((number[j] != 0) && ((number[j], j) < (number[i], i))) {}

	}
}

void Bread_unlock(int i)
{
	number[i] = 0;
}

void thread_NoLock(int t_id, int num_thread)
{
	for (int i = 0; i < MAX_NUMBER / num_thread; ++i)
	{
		SUM += 1;
	}
}

void thread_Mutex(int t_id, int num_thread)
{
	for (int i = 0; i < MAX_NUMBER / num_thread; ++i)
	{
		sum_lock.lock();
		SUM += 1;
		sum_lock.unlock();
	}
}

void thread_Bread(int t_id, int num_thread)
{
	for (int i = 0; i < MAX_NUMBER / num_thread; ++i)
	{
		Bread_lock(t_id, num_thread);
		SUM += 1;
		Bread_unlock(t_id);
	}
}

int main()
{
	for (int count = 1; count <= MAX_TRHEAD; count *= 2)
	{
		vector<thread> vec_thread;

		auto start_time = high_resolution_clock::now();

		for (int i = 0; i < count; ++i)
			vec_thread.emplace_back(thread_Bread, i, count);

		for (auto& t : vec_thread)
			t.join();


		auto end_time = high_resolution_clock::now();
		auto exec_time = end_time - start_time;

		cout << "Number of threads = " << count << " , ";
		cout << "Exec Time = " << duration_cast<milliseconds>(exec_time).count() << "ms , ";
		cout << "Sum = " << SUM << endl;


		// �ʱ�ȭ
		SUM = 0;

		for (auto& p : choosing)
			p = true;

		for (auto& p : number)
			p = 0;
	}
}