#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <chrono>
#include <atomic>
using namespace std;
using namespace chrono;

constexpr int NUM_TEST = 10000000;
constexpr int MAX_THREAD = 16;

///// Lock Free Queue , ABA문제 해결 [ STAMP 구현 ]

class NODE
{
public:
	volatile int key;
	NODE* volatile next;

	NODE()
	{
		key = 0;
		next = nullptr;
	}
	NODE(int key_value)
	{
		key = key_value;
		next = nullptr;
	}
	~NODE() {}
};

class null_mutex
{
public:
	void lock() {}
	void unlock() {}
};

// 중간값 문제로 인해 사용하지 않음
struct stamp_ptr
{
	NODE* ptr;
	int stamp;
};

class SPLFQUEUE
{
	long long head;
	long long tail;

public:
	long long make_stamp(NODE* p, int stamp)
	{
		// 두개를 합성하여 return => 64비트 주소에 stamp가 위로가고, 포인터가 아래로 간다.
		long long t = (unsigned int)stamp;
		t = (stamp << 32) | reinterpret_cast<unsigned int>(p);
		return t;
	}

	NODE* get_addr(long long sptr)
	{
		// 64비트의 sptr에서 주소를 뽑아야함 ( 뒤에 32 비트 )
		unsigned int a = sptr;
		return reinterpret_cast<NODE*>(a);
	}

	int get_stamp(long long sptr)
	{
		return sptr << 32;
	}

	SPLFQUEUE()
	{
		NODE* p = new NODE(0);
		head = make_stamp(p, 0);
		tail = make_stamp(p, 0);
	}
	~SPLFQUEUE() {}

	void Init()
	{
		NODE* ptr;
		// head는 stamp 포인터이기 때문에 NODE의 주소를 뽑아와야함
		while (get_addr(head)->next != nullptr) {
			ptr = get_addr(head)->next;
			get_addr(head)->next = get_addr(head)->next->next;
			delete ptr;
		}
		tail = head;
	}

	bool CAS(long long* addr, NODE* old_node, NODE* new_node, int old_stamp, int new_stamp)
	{
		long long old_v = make_stamp(old_node, old_stamp);
		long long new_v = make_stamp(new_node, new_stamp);

		return atomic_compare_exchange_strong( reinterpret_cast<atomic_llong*>(addr), &old_v, new_v );
	}

	// 32 비트용
	bool CAS(NODE* volatile* addr, NODE* old_node, NODE* new_node)
	{
		return atomic_compare_exchange_strong(
			reinterpret_cast<volatile atomic_int*>(addr),
			reinterpret_cast<int*>(&old_node),
			reinterpret_cast<int>(new_node));
	}

	void Enq(int key)
	{
		NODE* e = new NODE(key);
		while (true)
		{
			long long last = tail;
			NODE* next = get_addr(last)->next;
			if (last != tail)
				continue;
			if (next != nullptr)
			{
				int tail_stamp = get_stamp(last);
				CAS(&tail, get_addr(last), next, tail_stamp, tail_stamp + 1);
				continue;
			}

			// 32비트용 CAS 함수 사용하기
			if (false == CAS(&(get_addr(last))->next, nullptr, e))
				continue;

			int tail_stamp = get_stamp(last);
			CAS(&tail, get_addr(last), e, tail_stamp, tail_stamp + 1);
			return;
		}
	}

	int Deq()
	{
		while (true)
		{
			long long first = head;
			NODE* next = get_addr(first)->next;
			long long last = tail;
			NODE* lastnext = get_addr(last)->next;
			if (first != head)
				continue;

			if (last == first)
			{
				if (lastnext == nullptr)
				{
					return -1;
				}
				else
				{
					int tail_stamp = get_stamp(last);
					CAS(&tail, get_addr(last), lastnext, tail_stamp, tail_stamp + 1);
					continue;
				}
			}

			if (nullptr == next)
				continue;

			int result = next->key;

			int head_stamp = get_stamp(first);
			if (false == CAS(&head, get_addr(first), next, head_stamp, head_stamp+1))
				continue;

			get_addr(first)->next = nullptr;
			get_addr(first)->key = 0;
			delete get_addr(first);

			return result;
		}
	}

	void print_first_20()
	{
		NODE* curr = get_addr(head)->next;
		for (int i = 0; i < 20; ++i) {
			if (nullptr == curr) break;

			cout << curr->key << ", ";
			curr = curr->next;
		}
		cout << endl;
	}
};

SPLFQUEUE my_queue;

void worker(int num_thread) {
	for (int i = 0; i < NUM_TEST / num_thread; i++) {
		switch (rand() % 2)
		{
		case 0:
			my_queue.Enq(i);
			break;

		case 1:
			my_queue.Deq();
			break;

		default:
			cout << "Error\n";
			exit(-1);
		}
	}
}

int main()
{
	for (int num = 1; num <= MAX_THREAD; num = num * 2)
	{
		my_queue.Init();

		// 벤치마크 프로그램 실행
		vector<thread> workers;

		auto start_t = high_resolution_clock::now();

		for (int i = 0; i < num; ++i)
			workers.emplace_back(worker, num);

		for (auto& th : workers)
			th.join();

		auto end_t = high_resolution_clock::now();
		auto exec_time = end_t - start_t;

		cout << "Number of threads = " << num;
		cout << ",  Exec Time = " << duration_cast<milliseconds>(exec_time).count() << "ms" << endl;
		my_queue.print_first_20();
		cout << endl;
	}
}