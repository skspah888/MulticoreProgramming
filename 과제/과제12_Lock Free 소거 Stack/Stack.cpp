#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <chrono>
#include <atomic>
using namespace std;
using namespace chrono;

constexpr int NUM_TEST = 10000000;
constexpr int MAX_THREAD = 16;

///// Lock Free Stack

thread_local int thread_id;
int num_threads;

class NODE
{
public:
	volatile int key;
	NODE* volatile next;

	NODE()
	{
		key = 0;
		next = nullptr;
	}
	NODE(int key_value)
	{
		key = key_value;
		next = nullptr;
	}
	~NODE() {}
};

class null_mutex
{
public:
	void lock() {}
	void unlock() {}
};



class CSTACK
{
	NODE* volatile top;
	mutex s_lock;

public:
	CSTACK() : top(nullptr) { }
	~CSTACK()
	{
		clear();
	};

	void clear()
	{
		while (nullptr != top) {
			NODE* to_delete = top;
			top = top->next;
			delete to_delete;
		}
	}

	void Push(int key)
	{
		NODE* e = new NODE{ key };
		s_lock.lock();
		e->next = top;
		top = e;
		s_lock.unlock();
	}

	int Pop()
	{
		s_lock.lock();
		if (nullptr == top)
		{
			s_lock.unlock();
			return -1;
		}
		NODE* p = top;
		top = top->next;
		int value = p->key;
		s_lock.unlock();
		delete p;
		return value;
	}

	void display20()
	{
		NODE* ptr = top;
		for (int i = 0; i < 20; ++i) {
			if (nullptr == ptr) break;

			cout << ptr->key << ", ";
			ptr = ptr->next;
		}
		cout << endl;
	}
};


//////////////////////// Exchanger(Slot, State)
//////////////////////// 소거
constexpr unsigned char ST_EMPTY = 0;
constexpr unsigned char ST_WAITING = 1;
constexpr unsigned char ST_BUSY = 2;
constexpr int EMPTY_VALUE = 0;				// 초기값

class EXCHANGER
{
	atomic_int slot;			// SLOT을 따로 구현하지 않고 atomic_int로 대체
	unsigned char get_state()	// SLOT의 상태 [ 맨 앞의 2비트 ]
	{
		int t = slot;
		// 앞의 2비트를 리턴하는데, shift하면 sign비트까지 같이 shift된다.
		// 따라서, 쉬프트연산 후 0x3과 비트연산한다.
		return (t >> 30) & 0x3;		
	}
	int get_value()
	{
		int t = slot;
		return t & 0x7FFFFFFF;
	}
	bool SLOT_CAS(int old_state, int new_state, int old_value, int new_value)
	{
		int old_v = (old_state << 30 )+ old_value;
		int new_v = (new_state << 30 )+ new_value;
		return atomic_compare_exchange_strong(&slot, &old_v, new_v);
	}

public:
	EXCHANGER() : slot(0) {}
	int exchange(int value, bool* time_out, bool* busy)
	{
		for (int i=0; i< 1000; ++i) {
			unsigned char state = get_state();		// slot의 상태 확인
			switch (state)
			{
			case ST_EMPTY:		// Slot에 넣는다.
			{
				if (true == SLOT_CAS(ST_EMPTY, ST_WAITING, 0, value)) {
					// 무한정 대기를 막기위해 TimeOut필요
					int counter = 0;
					while (get_state() != ST_BUSY) {
						counter++;
						if (counter > 1000) {
							int ret = get_value();
							*time_out = true;
							return -1;
						}
					}
					int ret = get_value();
					slot = EMPTY_VALUE;
					return ret;
				}
				else			// 내가 Waiting하려했는데 누가 먼저 waiting하고있었다 [ 실패지만 오히려 좋은 상태 ]
					continue;
			}
			case ST_WAITING:
			{
				int old_v = get_value();
				if (true == SLOT_CAS(ST_WAITING, ST_BUSY, old_v, value)) {
					return old_v;
				}
				else
					continue;		// 실패했다는 것은, 다른 쓰레드가 BUSY로 바꿨다는 것 ( 다시시도함 )
			}
				
			case ST_BUSY:
				*busy = true;
				break;
			}
		}
		// timeout이 아닌 BUSY에서 실패해서 나가는 경우
		*busy = true;
		return -1;	
	}
};

thread_local int num_thread;

class EliminationArray
{
	int range;
	EXCHANGER exchanger[MAX_THREAD];

public:
	EliminationArray() { range = 1; }
	void init() { range = 1; }
	~EliminationArray() {}

	int Visit(int value, bool* time_out)
	{
		int slot = rand() % range;
		bool busy;
		int ret = exchanger[slot].exchange(value, time_out, &busy);
		if ((true == *time_out) && (range > 1)) range--;
		if ((true == busy) && (range <= num_thread / 2)) range++;
		// MAX RANGE is # of thread / 2

		return ret;
	}
};


//////////////////////// Lock Free
class LFSTACK
{
	NODE* volatile top;

public:
	LFSTACK() : top(nullptr) { }
	~LFSTACK()
	{
		clear();
	};

	void clear()
	{
		while (nullptr != top) {
			NODE* to_delete = top;
			top = top->next;
			delete to_delete;
		}
	}

	bool CAS(NODE* volatile* top, NODE* old_top, NODE* new_top)
	{
		// 64비트로 할거면 int -> longlong 
		return atomic_compare_exchange_strong(
			reinterpret_cast<atomic_int volatile*>(top),
			reinterpret_cast<int *>(&old_top),
			reinterpret_cast<int>(new_top)
		);
	}

	void Push(int key)
	{
		NODE* e = new NODE{ key };
		while (true)
		{
			NODE* first = top;
			e->next = first;
			if (true == CAS(&top, first, e))
				return;
		}
	}

	int Pop()
	{
		while (true)
		{
			NODE* first = top;
			if (nullptr == first) return -1;			// EMPTY!!

			NODE* next = first->next;
			if (first != top)
				continue;

			int value = first->key;
			if (true == CAS(&top, first, first->next))
			{
				//delete first;			// ABA문제로 일단 Delete하지 않는다
				return value;
			}
		}
	}

	void display20()
	{
		NODE* ptr = top;
		for (int i = 0; i < 20; ++i) {
			if (nullptr == ptr) break;

			cout << ptr->key << ", ";
			ptr = ptr->next;
		}
		cout << endl;
	}
};


//////////////////////// Lock Free Back Off 
class BACKOFF {
	int minDelay, maxDelay;
	int limit;
public:
	void init(int min, int max) 
	{
		minDelay = min;
		maxDelay = max;
		limit = min;
	}
	
	// Backoff
	void backoff1() {
		int delay = 0;
		if (limit != 0) delay = rand() % limit;
		limit *= 2;
		if (limit > maxDelay) limit = maxDelay;
		this_thread::sleep_for(chrono::microseconds(delay));;
	}

	// Better Backoff : current와 start가 계속 메모리 억세스를 하면서 충돌을 일으킴
	void backoff2()
	{
		int delay = 0;
		if (limit != 0)
			delay = rand() % limit;
		limit *= 2;
		if (limit > maxDelay)
			limit = maxDelay;
		int start, current;
		_asm RDTSC;
		_asm mov start, eax;
		do {
			_asm RDTSC;
			_asm mov current, eax;
		} while (current - start < delay);
	}

	void backoff()
	{
		int delay = 0;
		if (0 != limit) delay = rand() % limit;
		if (0 == delay) return;
		limit += limit;
		if (limit > maxDelay) limit = maxDelay;
		_asm mov eax, delay;
	myloop:
		_asm dec eax
		_asm jnz myloop;

	}
};


class LFBOSTACK
{
	BACKOFF bo;
	NODE* volatile top;
public:
	LFBOSTACK() : top(nullptr) 
	{
		bo.init(1, 10000);
	}
	~LFBOSTACK()
	{
		clear();
	};

	void clear()
	{
		bo.init(1, 10000);
		while (nullptr != top) {
			NODE* to_delete = top;
			top = top->next;
			delete to_delete;
		}
	}

	bool CAS(NODE* volatile* top, NODE* old_top, NODE* new_top)
	{
		// 64비트로 할거면 int -> longlong 
		return atomic_compare_exchange_strong(
			reinterpret_cast<atomic_int volatile*>(top),
			reinterpret_cast<int *>(&old_top),
			reinterpret_cast<int>(new_top)
		);
	}

	void Push(int key)
	{
		// Push와 Pop이 충돌할 수 있으니  BackOff 객체를 여기서 생성하는 것은 X
		NODE* e = new NODE{ key };
		while (true)
		{
			NODE* first = top;
			e->next = first;
			if (true == CAS(&top, first, e))
				return;
			bo.backoff();
		}
	}

	int Pop()
	{
		while (true)
		{
			NODE* first = top;
			if (nullptr == first) return -1;			// EMPTY!!

			NODE* next = first->next;
			if (first != top)
				continue;

			int value = first->key;
			if (true == CAS(&top, first, first->next))
			{
				//delete first;			// ABA문제로 일단 Delete하지 않는다
				return value;
			}
			bo.backoff();
		}
	}

	void display20()
	{
		NODE* ptr = top;
		for (int i = 0; i < 20; ++i) {
			if (nullptr == ptr) break;

			cout << ptr->key << ", ";
			ptr = ptr->next;
		}
		cout << endl;
	}
};


/// ///////////////////// Lock Free Elimination Stack
class LFELSTACK
{
	EliminationArray el;
	NODE* volatile top;
public:
	LFELSTACK() : top(nullptr)
	{
		el.init();
	}
	~LFELSTACK()
	{
		clear();
	};

	void clear()
	{
		el.init();
		while (nullptr != top) {
			NODE* to_delete = top;
			top = top->next;
			delete to_delete;
		}
	}

	bool CAS(NODE* volatile* top, NODE* old_top, NODE* new_top)
	{
		// 64비트로 할거면 int -> longlong 
		return atomic_compare_exchange_strong(
			reinterpret_cast<atomic_int volatile*>(top),
			reinterpret_cast<int*>(&old_top),
			reinterpret_cast<int>(new_top)
		);
	}

	void Push(int key)
	{
		// Push와 Pop이 충돌할 수 있으니  BackOff 객체를 여기서 생성하는 것은 X
		NODE* e = new NODE{ key };
		while (true)
		{
			NODE* first = top;
			e->next = first;
			if (true == CAS(&top, first, e))
				return;

			// backoff대신 사용
			bool time_out = false;
			int ret = el.Visit(key, &time_out);
			if (false == time_out)
			{
				if (-1 != ret)
					return;
			}
		}
	}

	int Pop()
	{
		while (true)
		{
			NODE* first = top;
			if (nullptr == first) return -1;			// EMPTY!!

			NODE* next = first->next;
			if (first != top)
				continue;

			int value = first->key;
			if (true == CAS(&top, first, first->next))
			{
				//delete first;	
				return value;
			}
			
			// backoff대신 사용 , 실패는 result가 -1을 반환한 것
			bool time_out = false;
			int ret = el.Visit(0, &time_out);
			if (false == time_out)
			{
				if (-1 != ret)
					return ret;
			}
		}
	}

	void display20()
	{
		NODE* ptr = top;
		for (int i = 0; i < 20; ++i) {
			if (nullptr == ptr) break;

			cout << ptr->key << ", ";
			ptr = ptr->next;
		}
		cout << endl;
	}
};


LFELSTACK my_stack;

void benchmark(int t_id, int threads) 
{
	num_thread = threads;
	const int loop = NUM_TEST / num_thread;
	thread_id = t_id;
	for (int i = 0; i < loop; ++i)
	{
		if ((rand() % 2 == 0) || (i < 1000 / num_thread))
			my_stack.Push(i);
		else
			my_stack.Pop();
	}
}

int main()
{
	// 벤치마크 프로그램 실행
	for (int num = 1; num <= MAX_THREAD; num = num * 2)
	{
		num_threads = num;
		vector<thread> workers;
		my_stack.clear();

		auto start_t = high_resolution_clock::now();

		for (int i = 0; i < num; ++i)
			workers.emplace_back(benchmark, i, num);

		for (auto& th : workers)
			th.join();

		auto end_t = high_resolution_clock::now();
		auto exec_time = end_t - start_t;

		cout << "Number of threads = " << num;
		cout << ",  Exec Time = " << duration_cast<milliseconds>(exec_time).count() << "ms" << endl;
		my_stack.display20();
		cout << endl;
	}
}