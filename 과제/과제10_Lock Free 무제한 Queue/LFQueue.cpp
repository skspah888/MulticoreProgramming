#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <chrono>
#include <atomic>
using namespace std;
using namespace chrono;

constexpr int NUM_TEST = 10000000;
constexpr int MAX_THREAD = 16;

///// Lock Free Queue

class NODE
{
public:
	int key;
	NODE* next;

	NODE()
	{
		key = 0;
		next = NULL;
	}
	NODE(int key_value)
	{
		key = key_value;
		next = nullptr;
	}
	~NODE() {}
};

class null_mutex
{
public:
	void lock() {}
	void unlock() {}
};

class LFQUEUE
{
	NODE* volatile head;
	NODE* volatile tail;

public:
	LFQUEUE()
	{
		head = tail = new NODE();
	}
	~LFQUEUE()
	{
	};

	void Init()
	{
		NODE* ptr;
		while (head->next != nullptr) {
			ptr = head->next;
			head->next = head->next->next;
			delete ptr;
		}
		tail = head;
	}

	bool CAS(NODE* volatile* addr, NODE* old_node, NODE* new_node)
	{
		return atomic_compare_exchange_strong(
			reinterpret_cast<volatile atomic_int *>(addr), 
			reinterpret_cast<int *>(&old_node), 
			reinterpret_cast<int>(new_node));
	}

	void Enq(int key)
	{
		NODE* e = new NODE(key);
		while (true)
		{
			NODE* last = tail;
			NODE* next = last->next;
			if (last != tail)
				continue;
			if (next != nullptr)
			{
				CAS(&tail, last, next);
				continue;
			}
			if (false == CAS(&last->next, nullptr, e))
				continue;

			CAS(&tail, last, e);
			return;
		}
	}

	int Deq()
	{
		while (true)
		{
			NODE* first = head;
			NODE* next = first->next;
			NODE* last = tail;
			NODE* lastnext = last->next;
			if (first != head)
				continue;

			if (last == first)
			{
				if (lastnext == nullptr)
				{
					return -1;
				}
				else
				{
					CAS(&tail, last, lastnext);
					continue;
				}
			}
			
			if (nullptr == next)
				continue;

			int result = next->key;
			if (false == CAS(&head, first, next))
				continue;
			first->next = nullptr;
			delete first;
			return result;
		}
	}

	void print_first_20()
	{
		NODE* curr = head->next;
		for (int i = 0; i < 20; ++i) {
			if (nullptr == curr) break;

			cout << curr->key << ", ";
			curr = curr->next;
		}
		cout << endl;
	}
};

LFQUEUE my_queue;

void worker(int num_thread) {


	for (int i = 0; i < NUM_TEST / num_thread; i++) {
		switch (rand() % 2)
		{
		case 0:
			my_queue.Enq(i);
			break;

		case 1:
			my_queue.Deq();
			break;

		default:
			cout << "Error\n";
			exit(-1);
		}
	}
}

int main()
{
	for (int num = 1; num <= MAX_THREAD; num = num * 2)
	{
		my_queue.Init();

		// 벤치마크 프로그램 실행
		vector<thread> workers;

		auto start_t = high_resolution_clock::now();

		for (int i = 0; i < num; ++i)
			workers.emplace_back(worker, num);

		for (auto& th : workers)
			th.join();

		auto end_t = high_resolution_clock::now();
		auto exec_time = end_t - start_t;

		cout << "Number of threads = " << num;
		cout << ",  Exec Time = " << duration_cast<milliseconds>(exec_time).count() << "ms" << endl;
		my_queue.print_first_20();
		cout << endl;
	}
}